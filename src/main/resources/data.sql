--Biodata
INSERT INTO public.m_biodata(fullname, mobile_phone, created_by, created_on, is_delete)
VALUES
    ('Galih Muhammad Ichsan', '081245768932', 1, now(),false),
    ('Jonathan Eric Gideon Siregar', '087564329867', 1, now(),false),
    ('Nikmatul Fitriya', '082358935123', 1, now(),false),
    ('Dyah Ayu Anggraini', '085436721875', 1, now(),false),
    ('Rony Paulian Nainggolen', '087435982909', 1, now(),false),
    ('Nyoman Paul Fernando Aro', '086543297956', 1, now(),false),
    ('Salma Salsabil Aliyah', '086347924766', 1, now(),false),
    ('Nabila Taqiyah', '087671474274', 1, now(),false),
    ('Haidar Chandra Bayu', '082742762752', 1, now(),false),
    ('Bagaskara Danu', '086223897303', 1, now(),false);


--Hak Akses
INSERT INTO public.m_role(name, code, created_by, created_on, is_delete)
VALUES
    ('Role Admin', 'ROLE_ADMIN', 2, now(), false),
    ('Role Pasien', 'ROLE_PASIEN', 2, now(), false),
    ('Role Dokter', 'ROLE_DOKTER', 2, now(), false);

--User
INSERT INTO public.m_user(biodata_id, role_id, email, password, is_locked, created_by, created_on, is_delete)
VALUES
    (1, 1, 'nyomangalih12@gmail.com', 'Galih001!', false, 1, now(), false);

--Specialization
INSERT INTO public.specialization(name, created_by, created_on, is_delete)
VALUES
    ('Spesialis Anak', 1, now(), false),
    ('Spesialis Kandungan', 1, now(), false),
    ('Spesialis Syaraf', 1, now(), false);

--Level Location
INSERT INTO public.level_location(name, abbreviation, created_by, created_on, is_delete)
VALUES
    ('Provinsi', 'Prov', 1, now(), false),
    ('Kabupaten', 'Kab', 1, now(), false),
    ('Kota', 'Kota', 1, now(), false),
    ('Kecamatan', 'Kec', 1, now(), false);

--Location
INSERT INTO public.location(name, parent_id, location_level_id, created_by, created_on, is_delete)
VALUES
    ('Semarang', null, 3, 1, now(), false),
    ('Gayamsari', 1, 4, 1, now(), false),
    ('Semarang Tengah', 1, 4, 1, now(), false),
    ('Tembalang', 1, 4, 1, now(), false),
    ('Jakarta Selatan', null, 3, 1, now(), false),
    ('Pasar Minggu', 5, 4, 1, now(), false),
    ('Tebet', 5, 4, 1, now(), false);

--Doctor
INSERT INTO public.doctor(biodata_id, str, created_by, created_on, is_delete)
VALUES
      (1, null, 1, now(), false),
      (2, null, 1, now(), false),
      (3, null, 1, now(), false),
      (4, null, 1, now(), false),
      (5, null, 1, now(), false);


--Doctor Treatment
INSERT INTO public.doctor_treatment(doctor_id, name, created_by, created_on, is_delete)
VALUES
      (1, 'Konsultasi Kesehatan Umum', 1, now(), false),
      (1, 'Medical Check up', 1, now(), false),
      (1, 'Perawatan Luka', 1, now(), false),

      (2, 'Konsultasi Kesehatan Umum', 1, now(), false),
      (2, 'Medical Check up', 1, now(), false),
      (2, 'Perawatan Luka', 1, now(), false),

      (3, 'Konsultasi Kesehatan Umum', 1, now(), false),
      (3, 'Medical Check up', 1, now(), false),
      (3, 'Perawatan Luka', 1, now(), false),

      (4, 'Konsultasi Kesehatan Umum', 1, now(), false),
      (4, 'Medical Check up', 1, now(), false),
      (4, 'Perawatan Luka', 1, now(), false),

      (5, 'Konsultasi Kesehatan Umum', 1, now(), false),
      (5, 'Medical Check up', 1, now(), false),
      (5, 'Perawatan Luka', 1, now(), false);

--Medical Facility Category
INSERT INTO public.medical_facility_category(name, created_by, created_on, is_delete)
VALUES
    ('RSUD', 1, now(), false),
    ('Puskesmas', 1, now(), false),
    ('Klinik', 1, now(), false);

--Medical Facility
INSERT INTO public.medical_facility(name, medical_facility_category_id, location_id, full_address , email, phone_code, phone, fax, created_by, created_on, is_delete)
VALUES
      ('RS Bhayangkara Semarang', 1, 2, 'Jl. Majapahit No.140, Gayamsari, Kec. Gayamsari, Kota Semarang, Jawa Tengah 50248', null, null, null, null, 1, now(), false),
      ('RS Hermina Pandanaran', 1, 3, 'Jl. Pandanaran Kel No.24, Pekunden, Kec. Semarang Tengah, Kota Semarang, Jawa Tengah 50134', null, null, null, null, 1, now(), false),
      ('RS Nasional Diponegoro', 1, 4, 'Jl. Prof. H. Soedarto S.H No.1, Tembalang, Kec. Tembalang, Kota Semarang, Jawa Tengah 50275', null, null, null, null, 1, now(), false),
      ('RSUD Pasar Minggu', 1, 6, 'Jl. TB Simatupang No.1, Ragunan, Kec. Pasar Minggu, Kota Jakarta Selatan, DKI Jakarta 12550', null, null, null, null, 1, now(), false),
      ('RSUD Tebet', 1, 7, 'Jl. Prof. DR. Soepomo SH No.54, Tebet Barat, Kec. Tebet, Kota Jakarta Selatan, DKI Jakarta 12810', null, null, null, null, 1, now(), false);

--Doctor Office
INSERT INTO public.doctor_office(doctor_id, medical_facility_id, specialization, start_date, end_date, created_by, created_on, is_delete)
VALUES
      (1, 1, 'Dokter Umum', '2017-10-20', now(), 1, now(), false),
      (1, 2, 'Dokter Umum', '201-10-20', now(), 1, now(), false),
      (1, 5, 'Dokter Umum', '2017-10-20', now(), 1, now(), false),
      (2, 2, 'Dokter Kandungan', '2016-05-1', now(), 1, now(), false),
      (2, 3, 'Dokter Kandungan', '2016-05-1', now(), 1, now(), false),
      (3, 3, 'Dokter Mata', '2017-01-10', now(), 1, now(), false),
      (3, 1, 'Dokter Mata', '2017-01-10', now(), 1, now(), false),
      (4, 4, 'Dokter THT', '2019-10-01', now(), 1, now(), false),
      (4, 5, 'Dokter THT', '2019-10-01', now(), 1, now(), false),
      (4, 4, 'Dokter THT', '2019-10-01', now(), 1, now(), false),
      (5, 5, 'Dokter Umum', '2018-06-10', now(), 1, now(), false);

--Doctor Office Treatment
INSERT INTO public.doctor_office_treatment(doctor_treatment_id, doctor_office_id, created_by, created_on, is_delete)
VALUES
    (1, 1, 1, now(), false),
    (5, 3, 1, now(), false),
    (2, 3, 1, now(), false),
    (4, 7, 1, now(), false),
    (2, 4, 1, now(), false);

--Customer Relation
INSERT INTO public.m_customer_relation(name, created_by, created_on, is_delete)
VALUES
    ('Ibu', 3, now(), false),
    ('Ayah', 3, now(), false),
    ('Anak', 3, now(), false),
    ('Suami', 3,now(), false),
    ('Istri', 3, now(), false),
    ('Diri Sendiri', 3, now(), false);

--Blood Group
INSERT INTO public.m_blood_group(code, descrtiption, created_by, created_on, is_delete)
VALUES
     ('A', '-', 3, now(), false),
     ('B', '-', 3, now(), false),
     ('AB', '-', 3, now(), false),
     ('O', '-', 3, now(), false);

--     Customer
INSERT INTO public.m_customer(biodata_id, dob, gender, blood_group_id, rhesus_type, height, weight, created_by, created_on, is_delete)
VALUES
    (7, '1993-10-21', 'P', 1 , 'Rh+' , 160.2 , 50.2 , 3, now(), false),
    (8, '1995-10-21', 'L', 1 , 'Rh-' , 170.2 , 60.2 , 3, now(), false);

--Customer Member
INSERT INTO public.m_customer_member(parent_biodata_id, customer_id, customer_relation_id, created_by, created_on, is_delete)
    VALUES
    (7, 1, 3, 3, now(), false),
    (8, 1, 5, 3, now(), false);

