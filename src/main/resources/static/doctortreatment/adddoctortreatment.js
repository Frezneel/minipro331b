$("#addBtnCancel").click(function(){
	$(".modal").modal("hide")
})

$("#addBtnCreate").click(function(){
	var name = $("#name").val().trim();
    var idDoctor = $("#doctorId").val();

	if(name == ""){
		$("#errName").text("Treatment tidak boleh kosong!");
		return;
	} else {
		$("#errName").text("");
	}

    DoctorTreatment(function(doctorTreatment) {
        console.log(doctorTreatment);
        var isNameExist = doctorTreatment.some(function(item) {
            return item.name.toLowerCase() === name.toLowerCase();
        });

        if (isNameExist) {
            $("#errName").text("Treatment sudah ada!");
            return;
        } else {
            $("#errName").text("");
        }

        var obj = {};
        obj.name = name;
        obj.doctor_id = idDoctor;

        var myJson = JSON.stringify(obj);

        $.ajax({
            url : "/api/doctortreatment",
            type : "POST",
            contentType : "application/json",
            data : myJson,
            success: function(data){
                    $(".modal").modal("hide")
                    refreshData(idDoctor);
            },
            error: function(){
                alert("Terjadi kesalahan")
            }
        });
    });
});

function DoctorTreatment(callback) {
var idDoctor = $("#doctorId").val();
    $.ajax({
        url: "/api/doctortreatment/?doctor_id=" + idDoctor,
        type: "GET",
        contentType: "application/json",
        success: function(doctorTreatment) {
            if (callback && typeof callback === "function") {
                callback(doctorTreatment);
            }
        },
        error: function(error) {
            if (callback && typeof callback === "function") {
                callback(error);
            }
        }
    });
}