$(document).ready(function() {
	getDoctorTreatmentById();

})

function getDoctorTreatmentById() {
	var id = $("#delId").val();
	$.ajax({
		url: "/api/doctortreatment/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#DoctorTreatmentDel").text(data.name);
		}
	})
}

$("#delBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#delBtnDelete").click(function() {
	var id = $("#delId").val();
	$.ajax({
		url : "/api/doctortreatment/"+ id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
				$(".modal").modal("hide");
                refreshData(idDokterGlobal);
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})