function GetAllDoctorTreatment(){
    $("#doctorTreatmentTable").html(
    	`<thead>
    		<tr>
    			<th>Nama</th>
    			<th>Action</th>
    		</tr>
    	</thead>
    	<tbody id="doctorTreatmentTBody"></tbody>
    	`
    );

    $.ajax({
		url : "/api/doctortreatment",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#doctorTreatmentTBody").append(
					`
					<tr>
					    <td>${data[i].name}</td>
                        <td>
                        	<button value="${data[i].id}" onClick="deleteDoctorTreatment(this.value)" class="btn btn-danger">
                        		<i class="bi-trash"></i>
                        	</button>
                        </td>
					</tr>
					`
				)
			}
		}
	});
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/doctortreatment/adddoctortreatment/",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create New Treatment");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function deleteDoctorTreatment(id){
	$.ajax({
		url: "/artis/deleteartis/" + id,
		type: "DELETE",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Artis");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

$(document).ready(function(){
	GetAllDoctorTreatment();

})