$("#addRoleBtnCancel").click(function(){
	$(".modal").modal("hide")
})

$("#addRoleBtnCreate").click(function(){
	var name = $("#inputNama").val();
	var code = $("#inputKode").val();

	var karakter =/[a-zA-Z0-9]/;

	if(name == ""){
	    $("#errName").text("Nama tidak boleh kosong!");
            if(code == ""){
                $("#errKode").text("Kode tidak boleh kosong!");
                return;
            } else if(!karakter.test(code)){
                $("#errKode").text("Kode tidak boleh kosong!");
                return;
            } else {
                $("#errKode").text("");
            }
		return;
	} else if(!karakter.test(name)){
	    $("#errName").text("Nama tidak boleh kosong!");
            if(code == ""){
                $("#errKode").text("Kode tidak boleh kosong!");
                return;
            } else if(!karakter.test(code)){
                $("#errKode").text("Kode tidak boleh kosong!");
                return;
            } else {
                $("#errKode").text("");
            }
        return;
	}else {
		$("#errName").text("");
	}

	if(code == ""){
        $("#errKode").text("Kode tidak boleh kosong!");
        return;
    } else if(!karakter.test(code)){
        $("#errKode").text("Kode tidak boleh kosong!");
        return;
    } else {
        $("#errKode").text("");
    }

    addRoleName(function(rolename) {
        var isNameExist = rolename.some(function(item) {
            return item.name.toLowerCase() === name.toLowerCase();
        });

        var isCodeExist = rolename.some(function(item) {
            return item.code === code;
        });

        if (isNameExist) {
            $("#errName").text("Nama sudah ada!");

        } else {
            $("#errName").text("");
        }

        if (isCodeExist) {
            $("#errKode").text("Kode sudah ada!");

        } else {
            $("#errKode").text("");
        }

        if(isNameExist || isCodeExist){
            return;
        }
	
        var obj = {};
        obj.name = name;
        obj.code = code;

        var myJson = JSON.stringify(obj);

        $.ajax({
            url : "/api/addrole",
            type : "POST",
            contentType : "application/json",
            data : myJson,
            success: function(data){
                    $(".modal").modal("hide")
                    getTabelKolomRole();
                    reloadData();
            },
            error: function(){
                alert("Terjadi kesalahan")
            }
        });
    });
})

function addRoleName(callback) {
    $.ajax({
        url: "/api/getallrole",
        type: "GET",
        contentType: "application/json",
        success: function(rolename) {
            if (callback && typeof callback === "function") {
                callback(rolename);
            }
        },
        error: function(error) {
            if (callback && typeof callback  === "function") {
                callback(error);
            }
        }
    });
}

