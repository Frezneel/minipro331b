$(document).ready(function() {
	getRoleById(); //dibedakan di setiap js delete, edit, add

})

function getRoleById() {
	var id = $("#delRoleId").val();
	$.ajax({
		url: "/api/getrole/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#idDel").text(data.id); //sesuaikan dengan id di html
			$("#nameDel").text(data.name);
		    $("#kodeDel").text(data.code);
		}
	})
}

$("#delRoleBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#delRoleBtnDelete").click(function() {
	var id = $("#delRoleId").val();

	$.ajax({
		url : "/api/deleterole/"+ id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
				$(".modal").modal("hide")
				getTabelKolomRole();
				location.reload();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})