var createdOn;
var createdBy;

function getEditRole() {
	var id = $("#editRoleId").val();
	$.ajax({
		url: "/api/getrole/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#nameEditInput").val(data.name);
			$("#inputKode").val(data.code);
			//taruh data kiri, ambil data kanan
			createdOn = data.created_on;
            createdBy = data.created_by;
            console.log(data);
		}
	});
}

$("#editRoleBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editRoleBtnCreate").click(function() {
	var id = $("#editRoleId").val();
	var name = $("#nameEditInput").val();
	var code = $("#inputKode").val();

    var karakter =/[a-zA-Z0-9]/;

    if(name == ""){
        $("#errName").text("Nama tidak boleh kosong!");
        return;
    } else if(!karakter.test(name)){
       $("#errName").text("Nama tidak boleh kosong!");
       return;
    } else {
        $("#errName").text("");
    }

    if(code == ""){
        $("#errKode").text("Kode tidak boleh kosong!");
        return;
    } else if(!karakter.test(code)){
        $("#errKode").text("Kode tidak boleh kosong!");
        return;
    } else {
        $("#errKode").text("");
    }

    editRoleName(function(data) {
        var isNameExist = data.some(function(item) {
            return item.name.toLowerCase() === name.toLowerCase();
        });

        if (isNameExist) {
            $("#errName").text("Nama sudah ada!");
            return;
        } else {
            $("#errName").text("");
        }

        var obj = {};
        obj.id = id;
        obj.name = name;
        obj.code = code;
        obj.created_on = createdOn;
        obj.created_by =  createdBy;

        var myJson = JSON.stringify(obj);

        console.log(myJson)

        $.ajax({
            url: "/api/editrole/" + id,
            type: "PUT",
            contentType: "application/json",
            data: myJson,
            success: function(data) {
                    $(".modal").modal("hide")
                    getEditRole();
                    reloadData();
            },
            error: function() {
                alert("Terjadi kesalahan")
            }
        });
	});
})

function editRoleName(callback) {
    $.ajax({
        url: "/api/getallrole",
        type: "GET",
        contentType: "application/json",
        success: function(rolename) {
            if (callback && typeof callback === "function") {
                callback(rolename);
            }
        },
        error: function(error) {
            if (callback && typeof callback === "function") {
                callback(error);
            }
        }
    });
}

$(document).ready(function() {
	getEditRole();
})