$(document).ready(function(){
    reloadData();
})

function reloadData(){
    console.log("test");
    var id = $("#saveIdDoctor").val();
    $.ajax({
        url : "/api/doctortreatment/?doctor_id=" + id,
        type : "GET",
        contentType : "application/json",
        success : function(data){
            viewImage(data[0].m_doctor.m_biodata.image);
            for(i = 0; i<data.length; i++){
                $("#isiTindakanMedis").append(
                `<p class="fs-6 mb-0 text-nowrap">- ${data[i].name}</p>`
                )
            }
        }
    });
}

function viewImage(dataImage){
    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext("2d");
    canvas.width = 300;
    canvas.height = 300;

    var image = document.getElementById("fileprofile");
    var imageData = 'data:image/png;base64,' + dataImage;

    image.onload = function(e) {
        ctx.drawImage(image,
            0, 0, image.width, image.height,
            0, 0, canvas.width, canvas.height
        );
        // create a new base64 encoding
        var resampledImage = new Image();
        resampledImage.src = canvas.toDataURL();
    };
    if(dataImage != null){
        document.getElementById("fileprofile").src = imageData;
    }else{
        document.getElementById("fileprofile").src = "../picture/img.png";
    }
}