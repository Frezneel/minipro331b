var idDokterGlobal;

function opsiDokter(){
    $.ajax({
        url : "/api/doctor",
        type : "GET",
        contentType : "application/json",
        success: function(data){
        console.log(data);
            for(i = 0; i<data.length; i++){
            $("#pilihDokter").append(
            `<option value="${data[i].id}">${data[i].m_biodata.fullname}</option>`
             )
            }
        }
    });
}

function tampilanDataTindakan(id){
    idDokterGlobal = id;
      console.log("test");
      $.ajax({
          url : "/api/doctortreatment/?doctor_id=" + id,
          type : "GET",
          contentType : "application/json",
          success: function(data){
          console.log(data);
          $("#isiTindakanMedis").html(``);
              for(i = 0; i<data.length; i++){
              $("#isiTindakanMedis").append(
              `<p class= "fs-6 mb-0 text-nowrap">- ${data[i].name}</p>`
               )
              }
          }
      });
}

function tampilanDataOffice(id){
      console.log("test");
      $.ajax({
          url : "/api/doctoroffice/?doctor_id=" + id,
          type : "GET",
          contentType : "application/json",
          success: function(data){
          console.log(data);
          $("#isiRiwayatPraktek").html(``);
              for(i = 0; i<data.length; i++){
              $("#isiRiwayatPraktek").append(
              `
              <p class="fs-6 mb-0 fw-normal">${data[i].m_medical_facility.name}</p>
                <div class="d-flex flex-row justify-content-between">
                    <div class="fs-6 px-2 fw-light">${data[i].specialization}</div>
                    <div class="fs-6 fw-light">${data[i].start_date} - ${data[i].end_date == null ? "Sekarang" :data[i].end_date}</div>
                </div>
              `
               )
              }
          }
      });
}

function tampilanDataPendidikan(id){
      console.log("test");
      $.ajax({
          url : "/api/doctoreducation/?doctor_id=" + id,
          type : "GET",
          contentType : "application/json",
          success: function(data){
          console.log(data);
          $("#isiRiwayatPendidikan").html(``);
              for(i = 0; i<data.length; i++){
              $("#isiRiwayatPendidikan").append(
              `
              <p class="fs-6 mb-0 fw-normal">${data[i].institution_name}</p>
                <div class="d-flex flex-row justify-content-between">
                    <div class="fs-6 px-2 fw-light">${data[i].major}</div>
                    <div class="fs-6 fw-light">${data[i].end_year}</div>
                </div>
              `
               )
              }
          }
      });
}

function tampilanBiodata(id){
      console.log("test biodata " + id);
      $.ajax({
          url : "/api/doctor/doctorID=" + id,
          type : "GET",
          contentType : "application/json",
          success: function(data){
              console.log(data);
              viewImage(data.m_biodata.image);
              $("#biodataId").html(``);
              $("#biodataId").append(
              `
              <p class="fs-5 mb-0 fw-bold text-center">
                  <button value="${data.id}" onClick="editPhoto(this.value)">
                      <i class="bi bi-pencil-square"></i>
                  </button>
              </p>
              <p class="fs-5 mb-0 fw-bold text-center">${data.m_biodata.fullname}</p>

              `
               )
          }
      });
}

function tampilanSpesialis(id){
    $.ajax({
        url : "/api/currentdoctorspecialization/doctorID=" + id,
        type : "GET",
        contentType : "application/json",
        success: function(data){
          console.log(data);
          $("#spesialisId").html(``);
              $("#spesialisId").append(
              `
              <p class="fs-6 mb-0 fw-light text-center">Dokter ${data.m_specialization.name}</p>
              `
               )
        }
    });
}

function tampilanJanji(id){
    $.ajax({
        url : "/api/appointment/doctorID=" + id,
        type : "GET",
        contentType : "application/json",
        success: function(data){
          console.log(data);
          $("#janjiId").html(``);
              $("#janjiId").append(
              `
              <div class="d-flex flex-row justify-content-between">
                <div class="col">
                  <p class="fs-5 fw-bold mb-1 text-success">
                      Janji
                  </p>
                </div>
                <div class="col-auto justify-content-end">
                  <p class="fs-5 mb-1 text-success">
                      ${data.length}
                  </p>
                </div>
              </div>
              <p class="fs-5 fw-bold mb-1 text-success">Obrolan / Konsultasi</p>
              `
               )
        }
    });
}

function editPhoto(id){
	$.ajax({
		url: "/doctor/editphoto/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Photo");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function viewImage(dataImage){
    var canvas = document.getElementById("canvasid");
    var ctx = canvas.getContext("2d");
    canvas.width = 300;
    canvas.height = 300;

    var image = document.getElementById("idImageProfile");
    var imageData = 'data:image/png;base64,' + dataImage;

    image.onload = function(e){
      ctx.drawImage(image,
      0, 0, image.width, image.height,
      0, 0, canvas.width, canvas.height
      );
      // new base64
      var resamlpedImage = new Image();
      resamlpedImage.src = canvas.toDataURL();
    };
    if(dataImage != null){
        document.getElementById("idImageProfile").src = imageData;
    }else{
        document.getElementById("idImageProfile").src = "../picture/img.png";
    }
}


var pilihDokter = document.getElementById("pilihDokter");
pilihDokter.onchange = function(){
    var id_doctor = pilihDokter.value;
    if(id_doctor != ""){
        idDokterGlobal = id_doctor;
        refreshData(id_doctor);
    }
}

function refreshData(id_doctor){
    tampilanDataTindakan(id_doctor);
    tampilanDataOffice(id_doctor);
    tampilanDataPendidikan(id_doctor);
    tampilanBiodata(id_doctor);
    tampilanSpesialis(id_doctor);
    tampilanJanji(id_doctor);
    GetAllDoctorTreatment(id_doctor);
}

$(document).ready(function(){
	opsiDokter();
})

function GetAllDoctorTreatment(id){
    $("#doctorTreatmentTable").html(
    	`
    	<div id = "doctorTreatmentTBody" class = "d-flex flex-wrap justify-content-start"></div>
    	`
    );

    $.ajax({
		url : "/api/doctortreatment/?doctor_id=" + id,
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#doctorTreatmentTBody").append(
					`
					<button ${data[i].name} value="${data[i].id}" onClick="deleteTreatment(this.value)" class="btn btn-success me-3 mt-3">
                    ${data[i].name}
                        <i class="bi bi-x-circle-fill bg-transparent" ></i>
                    </button>
					`
				)
			}
		}
	});
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/doctortreatment/adddoctortreatment/" + idDokterGlobal,
		type: "GET",
		contentType: "html",
		success: function(data){
		console.log(data);
			$(".modal-title").text("Create New Treatment");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function deleteTreatment(id){
	$.ajax({
		url: "/doctortreatment/deletedoctortreatment/" + id,
		type: "DELETE",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Treatment");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}