function ambilData(){
    var spesialis = $("#saveSpesialis").val();
    var lokasi = $("#saveLokasi").val();
    var namaDokter = $("#saveNamaDokter").val();
    var tindakan = $("#saveTindakan").val();
    $.ajax({
        url : "/api/doctor/getDoctorSearch/?specialization="+spesialis+"&location="+lokasi+"&fullname="+namaDokter+"&treatment="+tindakan,
        type : "GET",
        contentType : "application/json",
        success : function(data){
            var isSame;
            console.log(data);
            for(i = 0; i < data.length; i++){
                if(data[i].id != isSame){
                    isSame = data[i].id;
                    $("#listPage").append(`

                        <div class="card shadow-sm text-decoration-none text-black h-100 m-2" href="#">
                            <div class="card-body d-flex flex-column align-items-stretch">

                                <div class="row row-cols-2">

                                    <div class="col-9">
                                        <h5 class="card-title">${data[i].m_biodata.fullname}</h5>
                                        <h6 class="card-subtitle text-muted">${data[i].id}</h6>
                                        <h6 class="card-subtitle text-muted">doctorExperience Tahun pengalaman</h6>

                                        <div id="medicalFacility">
                                            <!--
                                                <section>
                                                    <img class="img-fluid" src="../icons/business_black_48dp.svg" alt="build icons"
                                                        style="height: 18px;">
                                                    Rs Mitra, (Jatiasih, Bekasi)
                                                </section>
                                            -->
                                        </div>

                                    </div>

                                    <div class="col-3 d-flex flex-column">
                                        <img class="img-fluid rounded w-100 img-doctor-style" src="http://localhost/img/${data.imagePath}" alt="img Doctor">
                                    </div>

                                </div>

                                <div class="row row-cols-2 align-items-end mt-auto pt-3">
                                    <div class="col-9">
                                        <button value="${data[i].id}" class="btn btn-cust-outline w-100" href="#" onclick="infoDetail(this.value)">info lebih banyak</button>
                                    </div>
                                    <div class="col-3 px-4">
                                        <div class="row d-flex flex-column">
                                            <div id="btnChat${data[i].id}" class="btn btn-cust-outline mb-2 disabled">Offline</div>
                                            <div class="btn btn-primary btn-cust-primary">Buat Janji</div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    `)
                }
            }
        }
    });
}

function kataKunciLocation(id){
    var lokasi = $("#saveLokasi").val();
    console.log(lokasi);
    $.ajax({
            url : "/api/location/" + lokasi,
            type : "GET",
            contentType : "application/json",
            success : function(data){
                console.log(data);
                $("#kataKunci").html(``);
                for(i = 0; i<data; i++){

                        $("#kataKunci").append(
                            `<h4>Hasil Pencarian Berdasarkan kata kunci: </h4>`
                        )

                }
            }
        });
}

//function kataKunci(){
//     var lokasi = $("#saveLokasi").val();
//    $("#kataKunci").html(``);
//    $("#kataKunci").append(`
//        <h4>Hasil Pencarian Berdasarkan kata kunci: ${lokasi}</h4>
//    `);
//}

function infoDetail(id){
    window.location.replace("http://localhost:8080/doctor/detail?id_dokter=" + id);
}

$(document).ready(function(){
    ambilData();
    kataKunciLocation();
})