$("#cariBtn").click(function(){
	$.ajax({
		url: "/doctor/caridokter",
        type: "GET",
        contentType: "html",
        success:function(data){
			$(".modal-title").text("Cari Dokter");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

$("#addBtnCancel").click(function(){
	$(".modal").modal("hide")
})


$("#addBtnUlang").click(function(){
    var originalModalContent = $(".modal .modal-content").html();

    $("#addBtnUlang").click(function(){
        $(".modal  .modal-content").html(originalModalContent);
        // Tampilkan modal
        $(".modal").modal("show");
    });
});

function opsilocation(){
    $.ajax({
        url : "/api/location",
        type : "GET",
        contentType : "application/json",
        success : function(data){
            for(i = 0; i<data.length; i++){
                $("#lokasi").append(
                `<option value="${data[i].id}">${data[i].name}</option>`
                )
            }
        }
    });
}

function opsiSpecialization(){
    $.ajax({
        url : "/api/specialization",
        type : "GET",
        contentType : "application/json",
        success : function(data){
            for(i = 0; i<data.length; i++){
                $("#spesialisasi").append(
                `<option value="${data[i].id}">${data[i].name}</option>`
                )
            }
        }
    });
}

function opsiTreatment(){
    $.ajax({
        url : "/api/doctortreatment",
        type : "GET",
        contentType : "application/json",
        success : function(data){
            for(i = 0; i<data.length; i++){
                $("#tindakanMedis").append(
                `<option value="${data[i].id}">${data[i].name}</option>`
                )
            }
        }
    });
}

$(document).ready(function(){
    opsilocation();
    opsiSpecialization();
    opsiTreatment();
})

