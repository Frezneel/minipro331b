$("#editBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editBtnCreate").click(function() {
    var id = $("#editId").val();
    var uploadFiles = $("#filePhoto").val();
    $.ajax({
        url: "/api/biodata/addImage/?id="+ id + "&uploadFiles=" + uploadFiles,
        type: "PUT",
        contentType: "application/json",
        success: function(data) {
                $(".modal").modal("hide");
                Specialization();
//              location.reload();
                reloadData();
        },
        error: function() {
            alert("Terjadi kesalahan")
        }
    });
});
