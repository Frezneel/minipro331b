$(document).ready(function() {
	GetSpecializationById();

})

function GetSpecializationById() {
	var id = $("#editId").val();
	$.ajax({
		url: "/api/specialization/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#nameEdit").val(data.name);
		}
	})
}

$("#editBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editBtnCreate").click(function() {
	var id = $("#editId").val();
	var name = $("#nameEdit").val().trim();

	if(name == ""){
    	$("#errName").text("Specialization tidak boleh kosong!");
    	return;
    } else {
    	$("#errName").text("");
    }

    Specialization(function(specialization) {
        var isNameExist = specialization.some(function(item) {
            return item.name === name;
        });

        if (isNameExist) {
            $("#errName").text("Nama sudah ada!");
            return;
        } else {
            $("#errName").text("");
        }

        var obj = {};
        obj.name = name;

        var myJson = JSON.stringify(obj);

        $.ajax({
            url: "/api/specialization/" + id,
            type: "PUT",
            contentType: "application/json",
            data: myJson,
            success: function(data) {
                    $(".modal").modal("hide");
                    location.reload();
            },
            error: function() {
                alert("Terjadi kesalahan")
            }
        });
	});
});

function Specialization(callback) {
    $.ajax({
        url: "/api/specialization",
        type: "GET",
        contentType: "application/json",
        success: function(specialization) {
            if (callback && typeof callback === "function") {
                callback(specialization);
            }
        },
        error: function(error) {
            if (callback && typeof callback === "function") {
                callback(error);
            }
        }
    });
}