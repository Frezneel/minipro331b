function GetAllSpecialization(){
    $("#specializationTable").html(
    	`<thead>
    		<tr>
    			<th>Nama</th>
    			<th>Action</th>
    		</tr>
    	</thead>
    	<tbody id="SpecializationList"></tbody>
    	`
    );

//    $.ajax({
//    	url : "/api/specialization",
//    	type : "GET",
//    	contentType : "application/json",
//    	success: function(data){
//    		for(i = 0; i<data.length; i++){
//    			$("#SpecializationTBody").append(
//    				`
//    				<tr>
//    					<td>${data[i].name}</td>
//                        <td>
//                            <button value="${data[i].id}" onClick="editSpecialization(this.value)" class="btn btn-warning">
//                            	<i class="bi-pencil-square"></i>
//                            </button>
//                            <button value="${data[i].id}" onClick="deleteSpecialization(this.value)" class="btn btn-danger">
//                            	<i class="bi-trash"></i>
//                            </button>
//                        </td>
//    				</tr>
//    				`
//    			)
//    		}
//    	}
//    });
}

function getPageable(currentPage, length){
    $.ajax({
    	    url : '/api/specializationmapped?page=' + currentPage + '&size=' + length,
    		type : 'GET',
    		contentType : 'application/json',
    		success : function(data) {

    			let table = "<table class='table table-bordered mt-3'>";
//    			table += "<tr> <th>Nama</th> <th>Action</th> </tr>"
    			for (let i = 0; i < data.specialization.length; i++) {
    				table += "<tr>";
    				table += "<td>" + data.specialization[i].name + "</td>";
    				table += "<td><button class='btn btn-primary btn-sm' value='" + data.specialization[i].id + "' onclick=editSpecialization(this.value)>Edit</button> <button class='btn btn-danger btn-sm' value='" + data.specialization[i].id + "' onclick=deleteSpecialization(this.value)>Delete</button></td>";
    				table += "</tr>";
    			}
    			table += "</table>";
    			table += "<br>"
    			table += '<nav aria-label="Page navigation">';
    			table += '<ul class="pagination">'
    			table += '<li class="page-item"><a class="page-link" onclick="getPageable(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
    			let index = 1;
    			for (let i = 0; i < data.totalPages; i++) {
    				table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="getPageable(' + i + ',' + length + ')">' + index + '</a></li>'
    				index++;
    			}
    		    table += '<li class="page-item"><a class="page-link" onclick="getPageable(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
    			table += '</ul>'
    			table += '</nav>';
    			$('#SpecializationList').html(table);
    		}
    	});
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/specialization/addspecialization",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create New Specialization");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function editSpecialization(id){
	$.ajax({
		url: "/specialization/editspecialization/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Specialization");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deleteSpecialization(id){
	$.ajax({
		url: "/specialization/deletespecialization/" + id,
		type: "DELETE",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Kelas");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function Search(request) {
    if(request.length > 0){
        $.ajax({
            url: "/api/searchspecialization=" + request,
            type: "GET",
            contentType: "application/json",
            success: function (result){
                let table = "<table class='table table-bordered mt-3'>"
//                table += "<tr> <th>Nama</th> <th>Action</th> </tr>"
                if (result.length > 0){
                    for(let i = 0; i < result.length; i++){
                        table += "<tr>";
                        table += "<td>" + result[i].name + "</td>";
                        table += "<td><button class='btn btn-primary btn-sm' value='" + result[i].id + "' onclick=editSpecialization(this.value)>Edit</button> <button class='btn btn-danger btn-sm' value='" + result[i].id + "' onclick=deleteSpecialization(this.value)>Delete</button></td>";
                        table += "</tr>";
                    }
                }
                else {
                    table += "<tr>";
                    table += "<td colspan='2' class='text-center'>No data</td>";
                    table += "</tr>";
                }
                table += "</table>";
                $('#SpecializationList').html(table);
            }
        });
    }
    else {
        getPageable(0, 5);
    }
}

//function GetSpecializationMapped(currentPage, length) {
//    $.ajax({
//	    url : '/api/specializationmapped?page=' + currentPage + '&size=' + length,
//		type : 'GET',
//		contentType : 'application/json',
//		success : function(data) {
//			let table = "<table class='table table-bordered mt-3'>";
//			table += "<tr> <th>Nama</th> <th>Action</th> </tr>"
//			for (let i = 0; i < data.specialization.length; i++) {
//				table += "<tr>";
//				table += "<td>" + data.specialization[i].name + "</td>";
//				table += "<td><button class='btn btn-primary btn-sm' value='" + data.specialization[i].id + "' onclick=editSpecialization(this.value)>Edit</button> <button class='btn btn-danger btn-sm' value='" + data.specialization[i].id + "' onclick=deleteSpecialization(this.value)>Delete</button></td>";
//				table += "</tr>";
//			}
//			table += "</table>";
//			table += "<br>"
//			table += '<nav aria-label="Page navigation">';
//			table += '<ul class="pagination">'
//			table += '<li class="page-item"><a class="page-link" onclick="GetSpecializationMapped(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
//			let index = 1;
//			for (let i = 0; i < data.totalPages; i++) {
//				table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="GetSpecializationMapped(' + i + ',' + length + ')">' + index + '</a></li>'
//				index++;
//			}
//		    table += '<li class="page-item"><a class="page-link" onclick="GetSpecializationMapped(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
//			table += '</ul>'
//			table += '</nav>';
//			$('#SpecializationList').html(table);
//		}
//	});
//}

//$("#searchBtn").click(function(){
//    var keyword = $("#search").val();
//    $.ajax({
//        url: "/api/searchspecialization=" + keyword,
//        type: "GET",
//        contentType: "html",
//        success:function(data){
//            $("#SpecializationList").html('');
//            for(i = 0; i<data.length; i++){
//            	$("#SpecializationList").append(
//            	    `
//            	    <tr>
//            			 <td>${data[i].name}</td>
//                         <td>
//                         <button value="${data[i].id}" onClick="editSpecialization(this.value)" class="btn btn-warning">
//                         <i class="bi-pencil-square"></i>
//                         </button>
//                         <button value="${data[i].id}" onClick="deleteSpecialization(this.value)" class="btn btn-danger">
//                         <i class="bi-trash"></i>
//                         </button>
//                         </td>
//            		</tr>
//            		`
//            	)
//            }
//        }
//    })
//})

$(document).ready(function(){
    GetAllSpecialization();
	getPageable(0, 5);


})