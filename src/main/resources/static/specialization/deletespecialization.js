$(document).ready(function() {
	getSpecializationById();

})

function getSpecializationById() {
	var id = $("#delId").val();
	$.ajax({
		url: "/api/specialization/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#SpecializationDel").text(data.name);
		}
	})
}

$("#delBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#delBtnDelete").click(function() {
	var id = $("#delId").val();
	$.ajax({
		url : "/api/specialization/"+ id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
				$(".modal").modal("hide")
				GetAllSpecialization();
				location.reload();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})