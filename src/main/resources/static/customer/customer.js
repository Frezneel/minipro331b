function getAllCustomerMember(){
    $("#customerTabel").html(
    	`<thead>
    		<tr>
    			<th>Nama</th>
    			<th>Relasi</th>
    			<th>Umur</th>
    			<th>Chat Online</>
    			<th>Janji Dokter</>
    			<th>Action</th>
    		</tr>
    	</thead>
    	<tbody id="customerTBody"></tbody>
    	`
    );

	$.ajax({
	    url : "/api/customermember/getAllCustomerMember",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#customerTBody").append(
					`
					<tr>
						<td>${data[i].m_biodata.fullname}</td>
						<td>${data[i].m_customer_relation.name}</td>
						<td>${((data[i].m_customer).dob)}</td>
						<td>
						    <button value="${data[i].id}" onClick="editcr(this.value)" class="btn btn-warning">
                        		<i class="bi-pencil-square"></i>
                        	</button>
                            <button value="${data[i].id}" onClick="deletecr(this.value)" class="btn btn-danger">
                            	<i class="bi-trash"></i>
                            </button>
						</td>
					</tr>

					`
				)
			}
		}
	});
}

$(document).ready(function(){
	getAllCustomerMember();
})