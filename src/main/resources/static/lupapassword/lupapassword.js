$("#btnKirimOtp").click(function(){
    buttonLoading(true);
    var emailInput = $("#emailLupaPassword").val();
    //Kombinasi dimana awal ada huruf/angka(tak hingga) + ada @ dan huruf/angka(tak hingga) + ada . dibelakangnya ada huruf/angka sebanyak 2 - 4
    var emailFormatValidation = /^[A-Za-z0-9_\-\.]+\@[A-Za-z0-9_\-\.]+\.[A-Za-z]{2,4}$/;

    if(emailInput === ""){
        $("#emailLupaPasswordErr").text("*Masukkan email!");
        buttonLoading(false);
        return
    }else if(!emailFormatValidation.test(emailInput)){
        $("#emailLupaPasswordErr").text("*Email tidak sesuai format!");
        buttonLoading(false);
        return
    }
    else{
        $("#emailLupaPasswordErr").text("");
    }
    $.ajax({
        url: "api/user/lupapassword/email=" + emailInput,
        type: "GET",
        contentType: "application/json",
        success: function(data){
           console.log(data);
           if(data != null){
               idUser = data.id;
               document.getElementById("emailLupaPasswordErr").className = "text-success";
               $("#emailLupaPasswordErr").text("*Email ditemukan!");
               newOTP(emailInput);
           }else{
               document.getElementById("emailLupaPasswordErr").className = "text-danger";
               $("#emailLupaPasswordErr").text("*Email tidak terdaftar");
               buttonLoading(false);
               return;
           }
        }
    });
})

var idUser;

function newOTP(email){
    $.ajax({
        url: "api/token/tokenToExpired/?email="+email+"&used_for=Lupa_Password",
        type: "PUT",
        contentType: "application/json",
        success: function(data){
           sentOTP(email);
        }
    });
}

function sentOTP(emailInput){
   var token = "";
   const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   const charactersLength = characters.length;
   for ( let i = 0; i < 8; i++ ) {
       token += characters.charAt(Math.floor(Math.random() * characters.length));
   }

   var otpMessage = "Kode OTP kamu: "+ token +". OTP ini bersifat RAHASIA harap tidak dibagikan ke siapapun. Silakan masukan kode ini untuk lupa password kamu.\n Jika merasa tidak melakukan lupa password abaikan mail ini Terima Kasih";
   var templateSent = {};
   templateSent.subject = "OTP Lupa Password PIN";
   templateSent.message = otpMessage;
   var myJson = JSON.stringify(templateSent);
   $.ajax({
       url: "api/mail/send/?mailTo=" + emailInput,
       type: "POST",
       data: myJson,
       contentType: "application/json",
       success: function(data){
          if(data === "Berhasil"){
              postToken(emailInput, token);
          }else{
              alert("Server gagal mengirimkan OTP");
              buttonLoading(false);
              return;
          }
       }
   });
}

function postToken(email, token){
    var data = {};
    data.email = email;
    data.token = token;
    data.is_expired = false;
    data.used_for = "Lupa_Password";
    var myJson = JSON.stringify(data);
    $.ajax({
        url : "/api/token/save",
        type : "POST",
        contentType : "application/json",
        data : myJson,
        success : function(data){
            moveToOtp(email);
        },
        error: function(){
            alert("Terjadi kesalahan")
            buttonLoading(false);
            return;
        }
    });
}

function moveToOtp(email){
    $.ajax({
        url: "/lupapassword/otp/"+email+"&"+idUser,
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal").modal("hide");
            $(".modal-title").text("Verifikasi E-Mail");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
}

function buttonLoading(keterangan){
    if(keterangan === true){
        document.getElementById("btnKirimOtp").disabled = true;
        document.getElementById("emailLupaPassword").disabled = true;
        document.getElementById("btnKirimOtp").innerHTML = "<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Loading...";
    }else{
        document.getElementById("btnKirimOtp").disabled = false;
        document.getElementById("emailLupaPassword").disabled = false;
        document.getElementById("btnKirimOtp").innerHTML = "Kirim OTP";
    }
}