// Mode Hidden atau Show
$("#eyeIconLupapasswordButton").click(function(){
    if($("#passwordLupasInput").attr("type") == "password"){
        $("#passwordLupasInput").attr("type", "text");
        $("#eyeIconLupapassword").removeClass("bi bi-eye-slash");
        $("#eyeIconLupapassword").addClass("bi bi-eye");
    }else if($("#passwordLupasInput").attr("type") == "text"){
        $("#passwordLupasInput").attr("type", "password");
        $("#eyeIconLupapassword").removeClass("bi bi-eye");
        $("#eyeIconLupapassword").addClass("bi bi-eye-slash");
    }
})
$("#eyeIconKonfirmasiLupapasswordButton").click(function(){
    if($("#passwordLupasKonfirmasi").attr("type") == "password"){
        $("#passwordLupasKonfirmasi").attr("type", "text");
        $("#eyeIconKonfirmasiLupapassword").removeClass("bi bi-eye-slash");
        $("#eyeIconKonfirmasiLupapassword").addClass("bi bi-eye");
    }else if($("#passwordLupasKonfirmasi").attr("type") == "text"){
        $("#passwordLupasKonfirmasi").attr("type", "password");
        $("#eyeIconKonfirmasiLupapassword").removeClass("bi bi-eye");
        $("#eyeIconKonfirmasiLupapassword").addClass("bi bi-eye-slash");
    }
})

$("#btnSetPassword").click(function(){
    buttonLoading(true);
    var password = $("#passwordLupasInput").val();
    var password_konfirmasi = $("#passwordLupasKonfirmasi").val();
    var boleanPassword = false;
    var boleanKonfirmasiPassword = false;
    var simbol = "";
    var angka = /[0-9]/;
    var hurufkecil = /[a-z]/;
    var hurufbesar = /[A-Z]/;
    var special = /[~`!@#$%^&*\(\)\-_+=\{\}\[\]|\\\;:\<>,.\"/?]/;
    var whitespace = /\s/;
    var textValidasi = "";

    if(password === ""){
        textValidasi += "*Masukkan password: "
    }

    //Strong password
    if(password.length < 8){
        if(textValidasi === ""){
            textValidasi += "*"
        }
        textValidasi += "Panjang minimal 8, ";
    }
    if(!angka.test(password)){
        if(textValidasi === ""){
            textValidasi += "*Harus "
        }
        textValidasi += "terdapat angka, ";
    }
    if(!hurufkecil.test(password)){
        if(textValidasi === ""){
            textValidasi += "*Harus "
        }
        textValidasi += "terdapat huruf kecil, ";
    }
    if(!hurufbesar.test(password)){
        if(textValidasi === ""){
            textValidasi += "*Harus "
        }
        textValidasi += "terdapat huruf besar, ";
    }
    if(!special.test(password)){
        if(textValidasi === ""){
            textValidasi += "*Harus "
        }
        textValidasi += "terdapat spesial karakter, ";
    }
    if(whitespace.test(password)){
        if(textValidasi === ""){
            textValidasi += "*"
        }
        textValidasi += "tidak boleh ada whitespace";
    }
    if (password_konfirmasi === ""){
        $("#passwordLupasKonfirmasiErr").text("*Konfimasi password tidak boleh kosong");
    }
    else if(password != password_konfirmasi){
        $("#passwordLupasKonfirmasiErr").text("*Password tidak sama");
    }else{
        $("#passwordLupasKonfirmasiErr").text("");
        boleanKonfirmasiPassword = true;
    }

    if(textValidasi != ""){
        $("#passwordLupasErr").text(textValidasi);
    }else{
        $("#passwordLupasErr").text("");
        boleanPassword = true;
    }

    if(boleanPassword == false || boleanKonfirmasiPassword == false){
        buttonLoading(false);
        return;
    }else if(boleanPassword == true && boleanKonfirmasiPassword == true){
        moveToUserData(password);
    }
})

function moveToUserData(password){
    var email = $("#saveEmail").val();
    var idUser = $("#saveIdUser").val();
    var obj = {};
    obj.password = password;
    var myJson = JSON.stringify(obj);

    $.ajax({
        url: "api/resetpassword/validasiPassword/" + idUser,
        type: "GET",
        contentType: "application/json",
        success: function(data){
            console.log(data);
            if(data.length < 1){
                $.ajax({
                    url: "api/user/lupa-password/email=" + email + "&password=" + password,
                    type: "GET",
                    async: true,
                    contentType: "application/json",
                    success: function(data){
                        if(data){
                            buttonLoading(false);
                            $("#passwordLupasErr").text("Password baru tidak boleh sama dengan password lama");
                            return;
                        }else{
                            $.ajax({
                                url: "api/user/lupa-password?email=" + email,
                                type: "POST",
                                data: myJson,
                                async: true,
                                contentType: "application/json",
                                success: function(data){
                                    var obj = {};
                                    obj.old_password = data;
                                    obj.new_password = password;
                                    obj.reset_for = "LUPA_PASSWORD";
                                    obj.created_by = idUser;
                                    var myJson = JSON.stringify(obj);
                                    $.ajax({
                                        url: "api/resetpassword/saveResetPassword",
                                        type: "POST",
                                        data: myJson,
                                        contentType: "application/json",
                                        success: function(data){
                                            moveInfo();
                                        }
                                    });
                                }
                            });
                        }
                    }
                })
            }else{
                var isSame = false;
                for(i=0; i<data.length; i++){
                    isSame = data[i].old_password === password ? true : false;
                    if(isSame == true){
                        break;
                    }
                    if(i == data.length - 1){
                        isSame = data[i].new_password === password ? true : false;
                    }
                    if(isSame == true){
                        break;
                    }
                }
                if(isSame){
                    buttonLoading(false);
                    $("#passwordLupasErr").text("Password baru tidak boleh sama dengan password sebelumnya");
                    return;
                }else{
                    $.ajax({
                        url: "api/user/lupa-password?email=" + email,
                        type: "POST",
                        data: myJson,
                        async: true,
                        contentType: "application/json",
                        success: function(data){
                            var obj = {};
                            obj.old_password = data;
                            obj.new_password = password;
                            obj.reset_for = "LUPA_PASSWORD";
                            obj.created_by = idUser;
                            var myJson = JSON.stringify(obj);
                            $.ajax({
                                url: "api/resetpassword/saveResetPassword",
                                type: "POST",
                                data: myJson,
                                contentType: "application/json",
                                success: function(data){
                                    moveInfo();
                                }
                            });
                        }
                    });
                }
            }
        }
    })


}

function moveInfo(){
    buttonLoading(false);
    $.ajax({
        url : "/lupapassword/info-lupapassword",
        type : "GET",
        contentType : "html",
        success: function(data){
             $(".modal").modal("hide");
             $(".modal-title").text("Informasi");
             $(".modal-body").html(data);
             $(".modal").modal("show");
        }
    });
}

function buttonLoading(keterangan){
    if(keterangan === true){
        document.getElementById("btnSetPassword").disabled = true;
        document.getElementById("btnSetPassword").innerHTML = "<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Loading...";
    }else{
        document.getElementById("btnSetPassword").disabled = false;
        document.getElementById("btnSetPassword").innerHTML = "Set Password";
    }
}
