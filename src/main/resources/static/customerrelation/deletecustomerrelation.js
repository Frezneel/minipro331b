$(document).ready(function() {
	getCustomerRelatationById(); //dibedakan di setiap js delete, edit, add

})

function getCustomerRelatationById() {
	var id   = $("#delCRId").val();
        $.ajax({
		url: "/api/customerrelation/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#nameDel").text(data.name); //sesuaikan dengan id di html
			if (data.active) { $("#activeDel").prop("checked", true); }
		}
	})
}

$("#delCRBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#delCrBtnDelete").click(function() {
	var id = $("#delCRId").val();
	$.ajax({
		url : "/api/customerrelation/"+ id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
				$(".modal").modal("hide")
				getAllCustomerRelation();
				location.reload();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})