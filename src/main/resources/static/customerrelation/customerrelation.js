function getAllCustomerRelation(){
    $("#customerrelationTabel").html(
    	`<thead>
    		<tr>
    			<th>Nama</th>
    			<th>Action</th>
    		</tr>
    	</thead>
    	<tbody id="customerrelationTBody"></tbody>
    	`
    );

	$.ajax({
	    url : "/api/customerrelation",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#customerrelationTBody").append(
					`
					<tr>
						<td>${data[i].name}</td>
						<td>
							<button value="${data[i].id}" onClick="editcr(this.value)" class="btn btn-warning">
								<i class="bi-pencil-square"></i>
							</button>
							<button value="${data[i].id}" onClick="deletecr(this.value)" class="btn btn-danger">
								<i class="bi-trash"></i>
							</button>
						</td>
					</tr>

					`
				)
			}
		}
	});
}
$("#searchBtn").click(function(){
    var kata = $("#search").val(); // ambi value dari kolom search
	$.ajax({
		url: "/api/searchcustomerrelation=" + kata,
		type: "GET",
		contentType: "html",
		success: function(data){
		    $("#customerrelationTBody").html(``); //untuk menghilangkan isi content dulu saat cari di search
			for(i = 0; i<data.length; i++){
            	$("#customerrelationTBody").append(
                    `
                    <tr>
                        <td>${data[i].name}</td>
                        <td>
                            <button value="${data[i].id}" onClick="editcr(this.value)" class="btn btn-warning">
                            <i class="bi-pencil-square"></i>
                            </button>
                            <button value="${data[i].id}" onClick="deletecr(this.value)" class="btn btn-danger">
                            <i class="bi-trash"></i>
                            </button>
                        </td>
                    </tr>

                    `

                )
            }
		}
	});
})
$("#addButton").click(function(){
	$.ajax({
		url: "/customerrelation/addcustomerrelation",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Tambah Nama");
			$(".modal-body").html(data);
			$(".modal").modal("show");

		}
	});
})

function editcr(id){
	$.ajax({
		url: "/customerrelation/editcustomerrelation/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Nama");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deletecr(id){
	$.ajax({
		url: "/customerrelation/deletecustomerrelation/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Nama");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}
$(document).ready(function(){
	getAllCustomerRelation();
})