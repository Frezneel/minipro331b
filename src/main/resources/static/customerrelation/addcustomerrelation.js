$("#addcrBtnCancel").click(function(){
	$(".modal").modal("hide")
})

$("#addcrBtnCreate").click(function(){
	var name = $("#name").val();

	var karakter =/[a-zA-Z0-9]/;

	if(name == ""){
		$("#errname").text("Nama tidak boleh kosong!");
		return;
	} else if (!karakter.test(name)){
	$("#errname").text("Nama tidak boleh kosong!");
    		return;
	}
	else {
		$("#errname").text("");
	}

	addCustomerRelationName(function(customerrelationname) {
        var isNameExist = customerrelationname.some(function(item) {

        return item.name.toLowerCase() === name.toLowerCase();
        });

        if (isNameExist) {
            $("#errname").text("Name Already Exists!");
            return;
        } else {
            $("#errname").text("");
        }

	    var obj = {};
	    obj.name = name;

	    var myJson = JSON.stringify(obj);

        $.ajax({
            url : "/api/customerrelation",
            type : "POST",
            contentType : "application/json",
            data : myJson,
            success: function(data){
                    $(".modal").modal("hide")
                    getAllCustomerRelation();
            },
            error: function(){
                alert("Terjadi kesalahan")
            }
        });

	});

})

function addCustomerRelationName(callback) {
    $.ajax({
        url: "/api/customerrelation",
        type: "GET",
        contentType: "application/json",
        success: function(customerrelationname) {
            if (callback && typeof callback === "function") {
                callback(customerrelationname);
            }
        },
        error: function(error) {
            if (callback && typeof callback  === "function") {
                callback(error);
            }
        }
    });
}