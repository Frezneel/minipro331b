function getTabelKolomCourier(){
    $("#courierTabel").html(
    		`<thead>
    			<tr>
    				<th>Nama</th>
    				<th>#</th>
    			</tr>
    		</thead>
    		<tbody id="courierTBody"></tbody>
    		`
    	);
}

function getPageable(page, ukuranHal){
    $.ajax({
        url : "/api/courier/mapped?halaman="+page+"&ukuranHal="+ukuranHal, //penamaan halaman dan ukuranHal menyamakan di API
        type : "GET",
        contentType : "application/json",
        success : function(data){
            $("#courierTBody").html(``);
            var halSekarang = data.halamanSekarang;
            var halTotal = data.totalHalaman;
            console.log(data);
            for(i = 0; i < data.courier.length; i++){
                $("#courierTBody").append(
                `
                <tr>
                    <td>${(data.courier[i]).name}</td>
                    <td>
                        <button value="${data.courier[i].id}" onClick="editCourier(this.value)" class="btn btn-primary">
                            <i class="bi-pencil-square">Ubah</i>
                        </button>
                        <button value="${data.courier[i].id}" onClick="deleteCourier(this.value)" class="btn btn-danger">
                            <i class="bi-trash">Hapus</i>
                        </button>
                    </td>
                </tr>
                `
                )
            }

            $("#halamanNav").html(``);
            if(halSekarang !=0){
                $("#halamanNav").append(
                `
                    <li class="page-item"><a class="page-link" href="#${(halSekarang)}" onClick="getPageable(${(halSekarang - 1)}, ${ukuranHal})"><<</a></li>
                `
                );
            }
            for(i = 0; i<data.totalHalaman; i++){
                if(i == data.halamanSekarang){
                    $("#halamanNav").append(
                    `
                        <li class="page-item disabled"><a class="page-link" onClick="getPageable(${(i)}, ${ukuranHal})">${(i+1)}</a></li>
                    `
                    );
                }else{
                    $("#halamanNav").append(
                    `
                        <li class="page-item"><a class="page-link" href="#${(i + 1)}" onClick="getPageable(${(i)}, ${ukuranHal})">${(i+1)}</a></li>
                    `
                    );
                }
            }

            if(halSekarang != (halTotal - 1) && halTotal> 0){
                $("#halamanNav").append(
                `
                    <li class="page-item"><a class="page-link" href="#${(halSekarang+2)}" onClick="getPageable(${(halSekarang + 1)}, ${ukuranHal})">>></a></li>
                `
                );
            }
        }
    });
}


$("#searchBtn").click(function(){
    var kata = $("#search").val(); // ambi value dari kolom search
	$.ajax({
		url: "/api/searchcourier=" + kata,
		type: "GET",
		contentType: "html",
		success: function(data){
		    $("#courierTBody").html(``); //untuk menghilangkan isi content dulu saat cari di search
			for(i = 0; i<data.length; i++){
            	$("#courierTBody").append(
                    `
                    <tr>
                        <td>${data[i].name}</td>
                        <td>
                            <button value="${data[i].id}" onClick="editCourier(this.value)" class="btn btn-primary">
                            <i>Ubah</i>
                            </button>
                            <button value="${data[i].id}" onClick="deleteCourier(this.value)" class="btn btn-danger">
                            <i>Hapus</i>
                            </button>
                        </td>
                    </tr>

                    `

                )
            }
            $("#halamanNav").html(``);
		}
	});
})

$("#addButton").click(function(){
	$.ajax({
		url: "/courier/addcourier",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Tambah Kurir");
			$(".modal-body").html(data);
			$(".modal").modal("show");

		}
	});
})


function editCourier(id){
	$.ajax({
		url: "/courier/editcourier/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Ubah Kurir");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deleteCourier(id){
	$.ajax({
		url: "/courier/deletecourier/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Hapus Kurir");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

$(document).ready(function(){
	getTabelKolomCourier();
	getPageable(0, 5);
})