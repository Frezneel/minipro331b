$("#addCourierBtnCancel").click(function(){
	$(".modal").modal("hide")
})

$("#addCourierBtnCreate").click(function(){
	var name = $("#inputNama").val();
    var karakter =/[a-zA-Z0-9]/;

	if(name == ""){
		$("#errName").text("Nama tidak boleh kosong!");
		return;
	} else if(!karakter.test(name)){
		$("#errName").text("Nama tidak boleh kosong!");
		return;
	}else {
		$("#errName").text("");
	}

    addCourierName(function(couriername) {
        console.log(name);
        var isNameExist = couriername.some(function(item) {
            return item.name.toLowerCase() === name.toLowerCase();
        });

        if (isNameExist) {
            $("#errName").text("Nama sudah ada!");
            return;
        }else {
            $("#errName").text("");
        }

        var obj = {};
        obj.name = name;

        var myJson = JSON.stringify(obj);
	
        $.ajax({
            url : "/api/addcourier",
            type : "POST",
            contentType : "application/json",
            data : myJson,
            success: function(data){
                $(".modal").modal("hide")
                getTabelKolomCourier();
                location.reload();
            },
            error: function(){
                alert("Terjadi kesalahan")
            }
        });
    });
});

function addCourierName(callback) {
    $.ajax({
        url: "/api/getallcourier",
        type: "GET",
        contentType: "application/json",
        success: function(couriername) {
            if (callback && typeof callback === "function") {
                callback(couriername);
            }
        },
        error: function(error) {
            if (callback && typeof callback === "function") {
                callback(error);
            }
        }
    });
}