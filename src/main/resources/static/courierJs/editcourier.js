

var createdOn;
var createdBy;

function getEditCourier() {
	var id = $("#editCourierId").val();
	$.ajax({
		url: "/api/getcourier/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#nameEditInput").val(data.name);
			//taruh data kiri, ambil data kanan
			createdOn = data.created_on;
            createdBy = data.created_by;
		}
	});
}

$("#editCourierBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editCourierBtnCreate").click(function() {
	var id = $("#editCourierId").val();
	var name = $("#nameEditInput").val();

	var karakter =/[a-zA-Z0-9]/;

	if(name == ""){
        $("#errName").text("Nama tidak boleh kosong!");
        return;
    } else if(!karakter.test(name)){
        $("#errName").text("Nama tidak boleh kosong!");
        return;
    } else {
        $("#errName").text("");
    }

	editCourierName(function(data) {
            var isNameExist = data.some(function(item) {
                return item.name.toLowerCase() === name.toLowerCase();
            });

            if (isNameExist) {
                $("#errName").text("Nama sudah ada!");
                return;
            } else {
                $("#errName").text("");
            }

        var obj = {};
        obj.id = id;
        obj.name = name;
        obj.created_on = createdOn;
        obj.created_by =  createdBy;

        var myJson = JSON.stringify(obj);

         $.ajax({
            url: "/api/editcourier/" + id,
            type: "PUT",
            contentType: "application/json",
            data: myJson,
            success: function(data) {
                    $(".modal").modal("hide")
                    getEditCourier();
                    location.reload();
            },
            error: function() {
                alert("Terjadi kesalahan")
            }
        });
    });
})

function editCourierName(callback) {
    $.ajax({
        url: "/api/getallcourier",
        type: "GET",
        contentType: "application/json",
        success: function(couriername) {

            if (callback && typeof callback === "function") {
                callback(couriername);
            }
        },
        error: function(error) {
            if (callback && typeof callback === "function") {
                callback(error);
            }
        }
    });
}

$(document).ready(function() {
	getEditCourier();
})