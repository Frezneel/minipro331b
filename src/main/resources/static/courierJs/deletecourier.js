$(document).ready(function() {
	getCourierById(); //dibedakan di setiap js delete, edit, add

})


function getCourierById() {
	var id = $("#delCourierId").val();
	$.ajax({
		url: "/api/getcourier/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#idDel").text(data.id); //sesuaikan dengan id di html
			$("#nameDel").text(data.name);
		}
	})
}

$("#delCourierBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#delCourierBtnDelete").click(function() {
	var id = $("#delCourierId").val();

	$.ajax({
		url : "/api/deletecourier/"+ id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
				$(".modal").modal("hide")
				getTabelKolomCourier();
				location.reload();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})