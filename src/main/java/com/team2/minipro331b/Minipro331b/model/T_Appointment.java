package com.team2.minipro331b.Minipro331b.model;

import jakarta.persistence.*;

import java.util.Date;

@Entity
@Table(name = "t_appointment")
public class T_Appointment extends BaseProperties{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "customer_id", insertable = false, updatable = false)
    public M_Customer m_customer;

    @Column(name = "customer_id", nullable = true)
    private Long customer_id;

    @ManyToOne
    @JoinColumn(name = "doctor_office_id", insertable = false, updatable = false)
    public T_Doctor_Office t_doctor_office;

    @Column(name = "doctor_office_id", nullable = true)
    private Long doctor_office_id;

    @ManyToOne
    @JoinColumn(name = "doctor_office_schedule_id",insertable = false, updatable = false)
    public M_Medical_Facility_Schedule m_medical_facility_schedule;

    @Column(name = "doctor_office_schedule_id",nullable = true)
    private Long doctor_office_schedule_id;

    @ManyToOne
    @JoinColumn(name = "doctor_office_treatment_id", insertable = false,updatable = false)
    public T_Doctor_Office_Treatment t_doctor_office_treatment;

    @Column(name = "doctor_office_treatment_id", nullable = true)
    private Long doctor_office_treatment_id;

    @Column(name = "appointment_date", nullable = true)
    private Date appointment_date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public M_Customer getM_customer() {
        return m_customer;
    }

    public void setM_customer(M_Customer m_customer) {
        this.m_customer = m_customer;
    }

    public Long getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Long customer_id) {
        this.customer_id = customer_id;
    }

    public T_Doctor_Office getT_doctor_office() {
        return t_doctor_office;
    }

    public void setT_doctor_office(T_Doctor_Office t_doctor_office) {
        this.t_doctor_office = t_doctor_office;
    }

    public Long getDoctor_office_id() {
        return doctor_office_id;
    }

    public void setDoctor_office_id(Long doctor_office_id) {
        this.doctor_office_id = doctor_office_id;
    }

    public M_Medical_Facility_Schedule getM_medical_facility_schedule() {
        return m_medical_facility_schedule;
    }

    public void setM_medical_facility_schedule(M_Medical_Facility_Schedule m_medical_facility_schedule) {
        this.m_medical_facility_schedule = m_medical_facility_schedule;
    }

    public Long getDoctor_office_schedule_id() {
        return doctor_office_schedule_id;
    }

    public void setDoctor_office_schedule_id(Long doctor_office_schedule_id) {
        this.doctor_office_schedule_id = doctor_office_schedule_id;
    }

    public T_Doctor_Office_Treatment getT_doctor_office_treatment() {
        return t_doctor_office_treatment;
    }

    public void setT_doctor_office_treatment(T_Doctor_Office_Treatment t_doctor_office_treatment) {
        this.t_doctor_office_treatment = t_doctor_office_treatment;
    }

    public Long getDoctor_office_treatment_id() {
        return doctor_office_treatment_id;
    }

    public void setDoctor_office_treatment_id(Long doctor_office_treatment_id) {
        this.doctor_office_treatment_id = doctor_office_treatment_id;
    }

    public Date getAppointment_date() {
        return appointment_date;
    }

    public void setAppointment_date(Date appointment_date) {
        this.appointment_date = appointment_date;
    }
}
