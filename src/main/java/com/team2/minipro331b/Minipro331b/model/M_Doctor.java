package com.team2.minipro331b.Minipro331b.model;

import jakarta.persistence.*;

@Entity
@Table(name = "doctor")
public class M_Doctor extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "biodata_id", insertable = false, updatable = false)
    public M_Biodata m_biodata;

    @Column(name = "biodata_id", nullable = true)
    private Long biodata_id;

    @Column(name = "str", length = 50, nullable = true)
    private String str;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public M_Biodata getM_biodata() {
        return m_biodata;
    }

    public void setM_biodata(M_Biodata m_biodata) {
        this.m_biodata = m_biodata;
    }

    public Long getBiodata_id() {
        return biodata_id;
    }

    public void setBiodata_id(Long biodata_id) {
        this.biodata_id = biodata_id;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }
}
