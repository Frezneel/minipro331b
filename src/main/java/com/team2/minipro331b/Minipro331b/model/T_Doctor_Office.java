package com.team2.minipro331b.Minipro331b.model;

import com.fasterxml.jackson.databind.ser.Serializers;

import jakarta.persistence.*;

import java.util.Date;

@Entity
@Table(name = "doctor_office")
public class T_Doctor_Office extends BaseProperties {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "doctor_id", insertable = false, updatable = false)
    public M_Doctor m_doctor;

    @Column(name = "doctor_id", nullable = true)
    private Long doctor_id;

    @ManyToOne
    @JoinColumn(name = "medical_facility_id", insertable = false, updatable = false)
    public M_Medical_Facility m_medical_facility;

    @Column(name = "medical_facility_id", nullable = true)
    private Long medical_facility_id;

    @Column(name = "specialization", length = 100, nullable = false)
    private String specialization;

    @Column(name = "start_date", nullable = false)
    private Date start_date;

    @Column(name = "end_date", nullable = false)
    private Date end_date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public M_Doctor getM_doctor() {
        return m_doctor;
    }

    public void setM_doctor(M_Doctor m_doctor) {
        this.m_doctor = m_doctor;
    }

    public Long getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(Long doctor_id) {
        this.doctor_id = doctor_id;
    }

    public M_Medical_Facility getM_medical_facility() {
        return m_medical_facility;
    }

    public void setM_medical_facility(M_Medical_Facility m_medical_facility) {
        this.m_medical_facility = m_medical_facility;
    }

    public Long getMedical_facility_id() {
        return medical_facility_id;
    }

    public void setMedical_facility_id(Long medical_facility_id) {
        this.medical_facility_id = medical_facility_id;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }
}
