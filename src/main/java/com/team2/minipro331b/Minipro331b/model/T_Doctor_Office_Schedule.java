package com.team2.minipro331b.Minipro331b.model;

import jakarta.persistence.*;

@Entity
@Table(name = "t_doctor_office_schedule")
public class T_Doctor_Office_Schedule extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "doctor_id", insertable = false, updatable = false)
    private M_Doctor m_doctor;

    @Column(name = "doctor_id", nullable = true)
    private Long doctor_id;

    @ManyToOne
    @JoinColumn(name = "medical_facility_schedule_id", insertable = false, updatable = false)
    private M_Medical_Facility_Schedule m_medical_facility_schedule;

    @Column(name = "medical_facility_schedule_id", nullable = true)
    private Long medical_facility_schedule_id;

    @Column(name = "slot", nullable = true)
    private Integer slot;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public M_Doctor getM_doctor() {
        return m_doctor;
    }

    public void setM_doctor(M_Doctor m_doctor) {
        this.m_doctor = m_doctor;
    }

    public Long getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(Long doctor_id) {
        this.doctor_id = doctor_id;
    }

    public M_Medical_Facility_Schedule getM_medical_facility_schedule() {
        return m_medical_facility_schedule;
    }

    public void setM_medical_facility_schedule(M_Medical_Facility_Schedule m_medical_facility_schedule) {
        this.m_medical_facility_schedule = m_medical_facility_schedule;
    }

    public Long getMedical_facility_schedule_id() {
        return medical_facility_schedule_id;
    }

    public void setMedical_facility_schedule_id(Long medical_facility_schedule_id) {
        this.medical_facility_schedule_id = medical_facility_schedule_id;
    }

    public Integer getSlot() {
        return slot;
    }

    public void setSlot(Integer slot) {
        this.slot = slot;
    }
}
