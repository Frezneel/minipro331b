package com.team2.minipro331b.Minipro331b.model;

import jakarta.persistence.*;

import java.sql.Blob;

@Entity
@Table(name = "m_biodata")
public class M_Biodata extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "fullname", nullable = true, length = 255)
    private String fullname;

    @Column(name = "mobile_phone", nullable = true, length = 15)
    private String mobile_phone;

    @Lob
    @Column(name = "image", nullable = true)
    private Blob image;

    @Column(name = "image_path", nullable = true, length = 255)
    private String image_path;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getMobile_phone() {
        return mobile_phone;
    }

    public void setMobile_phone(String mobile_phone) {
        this.mobile_phone = mobile_phone;
    }

    public Blob getImage() {
        return image;
    }

    public void setImage(Blob image) {
        this.image = image;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }
}
