package com.team2.minipro331b.Minipro331b.model;

import jakarta.persistence.*;

@Entity
@Table(name = "m_doctor_education")
public class M_Doctor_Education extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "doctor_id", insertable = false, updatable = false)
    public M_Doctor m_doctor;

    @Column(name = "doctor_id", nullable = true)
    private Long doctor_id;

    @ManyToOne
    @JoinColumn(name = "education_level_id", insertable = false, updatable = false)
    public M_Education_Level m_education_level;

    @Column(name = "education_level_id", nullable = true)
    private Long education_level_id;

    @Column(name = "institution_name", length = 100, nullable = true)
    private String institution_name;

    @Column(name = "major", length = 100, nullable = true)
    private String major;

    @Column(name = "start_year", length = 4, nullable = true)
    private String start_year;

    @Column(name = "end_year", length = 4, nullable = true)
    private String end_year;

    @Column(name = "is_last_education", nullable = true)
    private Boolean is_last_education;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public M_Doctor getM_doctor() {
        return m_doctor;
    }

    public void setM_doctor(M_Doctor m_doctor) {
        this.m_doctor = m_doctor;
    }

    public Long getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(Long doctor_id) {
        this.doctor_id = doctor_id;
    }

    public M_Education_Level getM_education_level() {
        return m_education_level;
    }

    public void setM_education_level(M_Education_Level m_education_level) {
        this.m_education_level = m_education_level;
    }

    public Long getEducation_level_id() {
        return education_level_id;
    }

    public void setEducation_level_id(Long education_level_id) {
        this.education_level_id = education_level_id;
    }

    public String getInstitution_name() {
        return institution_name;
    }

    public void setInstitution_name(String institution_name) {
        this.institution_name = institution_name;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getStart_year() {
        return start_year;
    }

    public void setStart_year(String start_year) {
        this.start_year = start_year;
    }

    public String getEnd_year() {
        return end_year;
    }

    public void setEnd_year(String end_year) {
        this.end_year = end_year;
    }

    public Boolean getIs_last_education() {
        return is_last_education;
    }

    public void setIs_last_education(Boolean is_last_education) {
        this.is_last_education = is_last_education;
    }
}
