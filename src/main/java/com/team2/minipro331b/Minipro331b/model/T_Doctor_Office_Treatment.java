package com.team2.minipro331b.Minipro331b.model;

import jakarta.persistence.*;

@Entity
@Table(name = "doctor_office_treatment")
public class T_Doctor_Office_Treatment extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "doctor_treatment_id", insertable = false, updatable = false)
    public T_Doctor_Treatment t_doctor_treatment;

    @Column(name = "doctor_treatment_id", nullable = true)
    private Long doctor_treatment_id;

    @ManyToOne
    @JoinColumn(name = "doctor_office_id", insertable = false, updatable = false)
    public T_Doctor_Office t_doctor_office;

    @Column(name = "doctor_office_id", nullable = true)
    private Long doctor_office_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public T_Doctor_Treatment getT_doctor_treatment() {
        return t_doctor_treatment;
    }

    public void setT_doctor_treatment(T_Doctor_Treatment t_doctor_treatment) {
        this.t_doctor_treatment = t_doctor_treatment;
    }

    public Long getDoctor_treatment_id() {
        return doctor_treatment_id;
    }

    public void setDoctor_treatment_id(Long doctor_treatment_id) {
        this.doctor_treatment_id = doctor_treatment_id;
    }

    public T_Doctor_Office getT_doctor_office() {
        return t_doctor_office;
    }

    public void setT_doctor_office(T_Doctor_Office t_doctor_office) {
        this.t_doctor_office = t_doctor_office;
    }

    public Long getDoctor_office_id() {
        return doctor_office_id;
    }

    public void setDoctor_office_id(Long doctor_office_id) {
        this.doctor_office_id = doctor_office_id;
    }
}
