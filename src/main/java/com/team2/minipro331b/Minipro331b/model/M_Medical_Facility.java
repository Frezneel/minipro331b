package com.team2.minipro331b.Minipro331b.model;
import jakarta.persistence.*;

@Entity
@Table(name = "medical_facility")
public class M_Medical_Facility extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false)
    private Long id;

    @Column(name = "name", length = 50, nullable = true)
    private String name;

    @ManyToOne
    @JoinColumn(name = "medical_facility_category_id", insertable = false, updatable = false)
    public M_Medical_Facility_Category m_medical_facility_category;

    @Column(name = "medical_facility_category_id", nullable = true)
    private Long medical_facility_category_id;

    @ManyToOne
    @JoinColumn(name = "location_id", insertable = false, updatable = false)
    public M_Location m_location;

    @Column(name = "location_id", nullable = true)
    private Long location_id;

    @Column(name = "full_address", nullable = true)
    private String full_address;

    @Column(name = "email", length = 100, nullable = true)
    private String email;

    @Column(name = "phone_code", length = 10, nullable = true)
    private String phone_code;

    @Column(name = "phone", length = 15, nullable = true)
    private String phone;

    @Column(name = "fax", length = 15, nullable = true)
    private String fax;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public M_Medical_Facility_Category getM_medical_facility_category() {
        return m_medical_facility_category;
    }

    public void setM_medical_facility_category(M_Medical_Facility_Category m_medical_facility_category) {
        this.m_medical_facility_category = m_medical_facility_category;
    }

    public Long getMedical_facility_category_id() {
        return medical_facility_category_id;
    }

    public void setMedical_facility_category_id(Long medical_facility_category_id) {
        this.medical_facility_category_id = medical_facility_category_id;
    }

    public M_Location getM_location() {
        return m_location;
    }

    public void setM_location(M_Location m_location) {
        this.m_location = m_location;
    }

    public Long getLocation_id() {
        return location_id;
    }

    public void setLocation_id(Long location_id) {
        this.location_id = location_id;
    }

    public String getFull_address() {
        return full_address;
    }

    public void setFull_address(String full_address) {
        this.full_address = full_address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_code() {
        return phone_code;
    }

    public void setPhone_code(String phone_code) {
        this.phone_code = phone_code;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }
}
