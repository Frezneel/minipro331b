package com.team2.minipro331b.Minipro331b.model;

import jakarta.persistence.*;

@Entity
@Table(name = "m_customer_member")
public class M_Customer_Member extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "parent_biodata_id", insertable = false, updatable = false)
    public M_Biodata m_biodata;

    @Column(name = "parent_biodata_id", nullable = true)
    private Long parent_biodata_id;

    @ManyToOne
    @JoinColumn(name = "customer_id", insertable = false, updatable = false)
    public  M_Customer m_customer;

    @Column(name = "customer_id", nullable = true)
    private Long customer_id;

    @ManyToOne
    @JoinColumn(name = "customer_relation_id", insertable = false, updatable = false)
    public M_Customer_Relation m_customer_relation;

    @Column(name = "customer_relation_id", nullable = true)
    private Long customer_relation_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public M_Biodata getM_biodata() {
        return m_biodata;
    }

    public void setM_biodata(M_Biodata m_biodata) {
        this.m_biodata = m_biodata;
    }

    public Long getParent_biodata_id() {
        return parent_biodata_id;
    }

    public void setParent_biodata_id(Long parent_biodata_id) {
        this.parent_biodata_id = parent_biodata_id;
    }

    public M_Customer getM_customer() {
        return m_customer;
    }

    public void setM_customer(M_Customer m_customer) {
        this.m_customer = m_customer;
    }

    public Long getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Long customer_id) {
        this.customer_id = customer_id;
    }

    public M_Customer_Relation getM_customer_relation() {
        return m_customer_relation;
    }

    public void setM_customer_relation(M_Customer_Relation m_customer_relation) {
        this.m_customer_relation = m_customer_relation;
    }

    public Long getCustomer_relation_id() {
        return customer_relation_id;
    }

    public void setCustomer_relation_id(Long customer_relation_id) {
        this.customer_relation_id = customer_relation_id;
    }
}
