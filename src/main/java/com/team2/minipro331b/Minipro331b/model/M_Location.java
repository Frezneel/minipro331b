package com.team2.minipro331b.Minipro331b.model;

import jakarta.persistence.*;

@Entity
@Table(name = "location")
public class M_Location extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false)
    private Long id;

    @Column(name = "name", length = 100, nullable = true)
    private String name;

    @ManyToOne
    @JoinColumn(name = "parent_id", insertable = false, updatable = false)
    public M_Location mLocation;

    @Column(name = "parent_id", nullable = true)
    private Long parent_id;

    @ManyToOne
    @JoinColumn(name = "location_level_id", insertable = false, updatable = false)
    public M_Level_Location m_level_location;

    @Column(name = "location_level_id", nullable = true)
    private Long location_level_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public M_Location getmLocation() {
        return mLocation;
    }

    public void setmLocation(M_Location mLocation) {
        this.mLocation = mLocation;
    }

    public Long getParent_id() {
        return parent_id;
    }

    public void setParent_id(Long parent_id) {
        this.parent_id = parent_id;
    }

    public M_Level_Location getM_level_location() {
        return m_level_location;
    }

    public void setM_level_location(M_Level_Location m_level_location) {
        this.m_level_location = m_level_location;
    }

    public Long getLocation_level_id() {
        return location_level_id;
    }

    public void setLocation_level_id(Long location_level_id) {
        this.location_level_id = location_level_id;
    }
}
