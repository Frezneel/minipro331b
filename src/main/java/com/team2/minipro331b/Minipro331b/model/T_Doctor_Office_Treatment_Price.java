package com.team2.minipro331b.Minipro331b.model;

import jakarta.persistence.*;


@Entity
@Table(name = "t_doctor_office_treatment_price")
public class T_Doctor_Office_Treatment_Price extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "doctor_office_treatment_id", insertable = false, updatable = false)
    private T_Doctor_Office_Treatment t_doctor_office_treatment;

    @Column(name = "doctor_office_treatment_id", nullable = true)
    private Long doctor_office_treatment_id;

    @Column(name = "price", nullable = true)
    private Double price;

    @Column(name = "price_start_from", nullable = true)
    private Double price_start_from;

    @Column(name = "price_until_from", nullable = true)
    private Double price_until_from;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDoctor_office_treatment_id() {
        return doctor_office_treatment_id;
    }

    public void setDoctor_office_treatment_id(Long doctor_office_treatment_id) {
        this.doctor_office_treatment_id = doctor_office_treatment_id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPrice_start_from() {
        return price_start_from;
    }

    public void setPrice_start_from(Double price_start_from) {
        this.price_start_from = price_start_from;
    }

    public Double getPrice_until_from() {
        return price_until_from;
    }

    public void setPrice_until_from(Double price_until_from) {
        this.price_until_from = price_until_from;
    }
}
