package com.team2.minipro331b.Minipro331b.model;

import jakarta.persistence.*;

@Entity
@Table(name = "medical_facility_id")
public class M_Medical_Facility_Schedule extends BaseProperties {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "medical_facility_id", insertable = false, updatable = false)
    public M_Medical_Facility m_medical_facility;

    @Column(name = "medical_facility_id", nullable = true)
    private Long medical_facility_id;

    @Column(name = "day",nullable = true)
    private String day;

    @Column(name = "time_schedule_start", nullable = true)
    private String time_schedule_start;

    @Column(name = "time_schedule_end", nullable = true)
    private String time_schedule_end;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public M_Medical_Facility getM_medical_facility() {
        return m_medical_facility;
    }

    public void setM_medical_facility(M_Medical_Facility m_medical_facility) {
        this.m_medical_facility = m_medical_facility;
    }

    public Long getMedical_facility_id() {
        return medical_facility_id;
    }

    public void setMedical_facility_id(Long medical_facility_id) {
        this.medical_facility_id = medical_facility_id;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTime_schedule_start() {
        return time_schedule_start;
    }

    public void setTime_schedule_start(String time_schedule_start) {
        this.time_schedule_start = time_schedule_start;
    }

    public String getTime_schedule_end() {
        return time_schedule_end;
    }

    public void setTime_schedule_end(String time_schedule_end) {
        this.time_schedule_end = time_schedule_end;
    }
}
