package com.team2.minipro331b.Minipro331b.model;

import jakarta.persistence.*;

import java.util.Date;

@Entity
@Table(name = "m_user")
public class M_User extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "biodata_id", insertable = false, updatable = false)
    private M_Biodata m_biodata;

    @Column(name = "biodata_id", nullable = true)
    private Long biodata_id;

    @ManyToOne
    @JoinColumn(name = "role_id", insertable = false, updatable = false)
    private M_Role m_role;

    @Column(name = "role_id", nullable = true)
    private Long role_id;

    @Column(name = "email", nullable = true, length = 100)
    private String email;

    @Column(name = "password", nullable = true, length = 255)
    private String password;

    @Column(name = "login_attempt", nullable = true)
    private Integer login_attempt;

    @Column(name = "is_locked", nullable = true)
    private Boolean is_locked;

    @Column(name = "last_login", nullable = true)
    private Date last_login;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public M_Biodata getM_biodata() {
        return m_biodata;
    }

    public void setM_biodata(M_Biodata m_biodata) {
        this.m_biodata = m_biodata;
    }

    public Long getBiodata_id() {
        return biodata_id;
    }

    public void setBiodata_id(Long biodata_id) {
        this.biodata_id = biodata_id;
    }

    public M_Role getM_role() {
        return m_role;
    }

    public void setM_role(M_Role m_role) {
        this.m_role = m_role;
    }

    public Long getRole_id() {
        return role_id;
    }

    public void setRole_id(Long role_id) {
        this.role_id = role_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getLogin_attempt() {
        return login_attempt;
    }

    public void setLogin_attempt(Integer login_attempt) {
        this.login_attempt = login_attempt;
    }

    public Boolean getIs_locked() {
        return is_locked;
    }

    public void setIs_locked(Boolean is_locked) {
        this.is_locked = is_locked;
    }

    public Date getLast_login() {
        return last_login;
    }

    public void setLast_login(Date last_login) {
        this.last_login = last_login;
    }
}
