package com.team2.minipro331b.Minipro331b.model;

import jakarta.persistence.*;

@Entity
@Table(name = "doctor_treatment")
public class T_Doctor_Treatment extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "doctor_id", insertable = false, updatable = false)
    public M_Doctor m_doctor;

    @Column(name = "doctor_id", nullable = true)
    private Long doctor_id;

    @Column(name = "name", length = 50, nullable = true)
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public M_Doctor getM_doctor() {
        return m_doctor;
    }

    public void setM_doctor(M_Doctor m_doctor) {
        this.m_doctor = m_doctor;
    }

    public Long getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(Long doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
