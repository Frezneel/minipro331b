package com.team2.minipro331b.Minipro331b.model;
import jakarta.persistence.*;

@Entity
@Table(name = "cari_dokter")
public class Cari_Dokter extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "location_id", insertable = false, updatable = false)
    public M_Location m_location;

    @Column(name = "location_id")
    private Long location_id;

    @ManyToOne
    @JoinColumn(name = "doctor_id", insertable = false, updatable = false)
    public M_Doctor m_doctor;

    @Column(name = "doctor_id")
    private Long doctor_id;

    @ManyToOne
    @JoinColumn(name = "specialization_id", insertable = false, updatable = false)
    public M_Specialization m_specialization;

    @Column(name = "specialization_id")
    private Long specialization_id;

    @ManyToOne
    @JoinColumn(name = "doctor_treatment_id", insertable = false, updatable = false)
    public T_Doctor_Treatment t_doctor_treatment;

    @Column(name = "doctor_treatment_id")
    private Long doctor_treatment_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public M_Location getM_location() {
        return m_location;
    }

    public void setM_location(M_Location m_location) {
        this.m_location = m_location;
    }

    public Long getLocation_id() {
        return location_id;
    }

    public void setLocation_id(Long location_id) {
        this.location_id = location_id;
    }

    public M_Doctor getM_doctor() {
        return m_doctor;
    }

    public void setM_doctor(M_Doctor m_doctor) {
        this.m_doctor = m_doctor;
    }

    public Long getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(Long doctor_id) {
        this.doctor_id = doctor_id;
    }

    public M_Specialization getM_specialization() {
        return m_specialization;
    }

    public void setM_specialization(M_Specialization m_specialization) {
        this.m_specialization = m_specialization;
    }

    public Long getSpecialization_id() {
        return specialization_id;
    }

    public void setSpecialization_id(Long specialization_id) {
        this.specialization_id = specialization_id;
    }

    public T_Doctor_Treatment getT_doctor_treatment() {
        return t_doctor_treatment;
    }

    public void setT_doctor_treatment(T_Doctor_Treatment t_doctor_treatment) {
        this.t_doctor_treatment = t_doctor_treatment;
    }

    public Long getDoctor_treatment_id() {
        return doctor_treatment_id;
    }

    public void setDoctor_treatment_id(Long doctor_treatment_id) {
        this.doctor_treatment_id = doctor_treatment_id;
    }
}
