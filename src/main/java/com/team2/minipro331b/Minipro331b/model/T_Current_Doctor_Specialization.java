package com.team2.minipro331b.Minipro331b.model;

import jakarta.persistence.*;

@Entity
@Table(name = "t_current_doctor_specialization")
public class T_Current_Doctor_Specialization extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "doctor_id", insertable = false, updatable = false)
    private M_Doctor m_doctor;

    @Column(name = "doctor_id", nullable = true)
    private Long doctor_id;

    @ManyToOne
    @JoinColumn(name = "specialization_id", insertable = false, updatable = false)
    private M_Specialization m_specialization;

    @Column(name = "specialization_id", nullable = true)
    private Long specialization_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public M_Doctor getM_doctor() {
        return m_doctor;
    }

    public void setM_doctor(M_Doctor m_doctor) {
        this.m_doctor = m_doctor;
    }

    public Long getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(Long doctor_id) {
        this.doctor_id = doctor_id;
    }

    public M_Specialization getM_specialization() {
        return m_specialization;
    }

    public void setM_specialization(M_Specialization m_specialization) {
        this.m_specialization = m_specialization;
    }

    public Long getSpecialization_id() {
        return specialization_id;
    }

    public void setSpecialization_id(Long specialization_id) {
        this.specialization_id = specialization_id;
    }
}
