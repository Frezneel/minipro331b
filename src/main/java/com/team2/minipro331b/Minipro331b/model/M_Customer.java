package com.team2.minipro331b.Minipro331b.model;

import jakarta.persistence.*;

import java.util.Date;

@Entity
@Table(name = "m_customer")
public class M_Customer extends BaseProperties {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "biodata_id", insertable = false, updatable = false )
    public M_Biodata m_biodata;

    @Column(name = "biodata_id", nullable = true)
    private Long biodata_id;

    @Column(name = "dob", nullable = true)
    private Date dob;

    @Column(name = "gender", nullable = true, length = 1 )
    private String gender;

    @ManyToOne
    @JoinColumn(name = "blood_group_id",insertable = false, updatable = false)
    public M_Blood_Group m_blood_group;

    @Column(name = "blood_group_id", nullable = true)
    private Long blood_group_id;

    @Column(name = "rhesus_type", nullable = true, length = 5)
    private String rhesus_type;

    @Column(name = "height", nullable = true)
    private Double height;

    @Column(name = "weight", nullable = true)
    private Double weight;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public M_Biodata getM_biodata() {
        return m_biodata;
    }

    public void setM_biodata(M_Biodata m_biodata) {
        this.m_biodata = m_biodata;
    }

    public Long getBiodata_id() {
        return biodata_id;
    }

    public void setBiodata_id(Long biodata_id) {
        this.biodata_id = biodata_id;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public M_Blood_Group getM_blood_group() {
        return m_blood_group;
    }

    public void setM_blood_group(M_Blood_Group m_blood_group) {
        this.m_blood_group = m_blood_group;
    }

    public Long getBlood_group_id() {
        return blood_group_id;
    }

    public void setBlood_group_id(Long blood_group_id) {
        this.blood_group_id = blood_group_id;
    }

    public String getRhesus_type() {
        return rhesus_type;
    }

    public void setRhesus_type(String rhesus_type) {
        this.rhesus_type = rhesus_type;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }
}
