package com.team2.minipro331b.Minipro331b.model;

import jakarta.persistence.*;

@Entity
@Table(name = "m_profil_doctor")
public class ProfilDoctor extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "doctor_id", insertable = false, updatable = false)
    public M_Doctor m_doctor;

    @Column(name = "doctor_id", nullable = true)
    private Long doctor_id;

    @ManyToOne
    @JoinColumn(name = "doctor_treatment_id", insertable = false, updatable = false)
    public T_Doctor_Treatment t_doctor_treatment;

    @Column(name = "doctor_treatment_id", nullable = true)
    private Long doctor_treatment_id;

    @ManyToOne
    @JoinColumn(name = "doctor_education_id", insertable = false, updatable = false)
    public M_Doctor_Education m_doctor_education;

    @Column(name = "doctor_education_id", nullable = true)
    private Long doctor_education_id;

    @ManyToOne
    @JoinColumn(name = "doctor_office_id", insertable = false, updatable = false)
    public T_Doctor_Office t_doctor_office;

    @Column(name = "doctor_office_id", nullable = true)
    private Long doctor_office_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public M_Doctor getM_doctor() {
        return m_doctor;
    }

    public void setM_doctor(M_Doctor m_doctor) {
        this.m_doctor = m_doctor;
    }

    public Long getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(Long doctor_id) {
        this.doctor_id = doctor_id;
    }

    public T_Doctor_Treatment getT_doctor_treatment() {
        return t_doctor_treatment;
    }

    public void setT_doctor_treatment(T_Doctor_Treatment t_doctor_treatment) {
        this.t_doctor_treatment = t_doctor_treatment;
    }

    public Long getDoctor_treatment_id() {
        return doctor_treatment_id;
    }

    public void setDoctor_treatment_id(Long doctor_treatment_id) {
        this.doctor_treatment_id = doctor_treatment_id;
    }

    public M_Doctor_Education getM_doctor_education() {
        return m_doctor_education;
    }

    public void setM_doctor_education(M_Doctor_Education m_doctor_education) {
        this.m_doctor_education = m_doctor_education;
    }

    public Long getDoctor_education_id() {
        return doctor_education_id;
    }

    public void setDoctor_education_id(Long doctor_education_id) {
        this.doctor_education_id = doctor_education_id;
    }

    public T_Doctor_Office getT_doctor_office() {
        return t_doctor_office;
    }

    public void setT_doctor_office(T_Doctor_Office t_doctor_office) {
        this.t_doctor_office = t_doctor_office;
    }

    public Long getDoctor_office_id() {
        return doctor_office_id;
    }

    public void setDoctor_office_id(Long doctor_office_id) {
        this.doctor_office_id = doctor_office_id;
    }
}
