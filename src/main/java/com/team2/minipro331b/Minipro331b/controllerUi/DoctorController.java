package com.team2.minipro331b.Minipro331b.controllerUi;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "doctor")
public class DoctorController {

    @RequestMapping("caridokter")
    public String findDoctor() {
        return "doctor/caridokter";
    }

    @RequestMapping("result")
    public String halamanCariDokter(@RequestParam(value = "lokasi", defaultValue = "0")Long lokasi, @RequestParam(value = "namaDokter", defaultValue = "")String namaDokter,
                                    @RequestParam("spesialis")Long spesialis, @RequestParam(value = "tindakan", defaultValue = "0")Long tindakan,
                                    Model model) {
        model.addAttribute("lokasi", lokasi);
        model.addAttribute("namaDokter", namaDokter);
        model.addAttribute("spesialis", spesialis);
        model.addAttribute("tindakan", tindakan);
        return "doctor/halamancaridokter";
    }

    @RequestMapping("detail")
    public ModelAndView halamanDetailDokter(@RequestParam("id_dokter") Long id_dokter, Model model){
        model.addAttribute("id_dokter", id_dokter);
        return new ModelAndView("doctor/detail-dokter");
    }
    @RequestMapping("profilsharedlayout")
    public String halamanProfilSharedLayout(){
        return "doctor/profilsharedlayout";
    }


    @RequestMapping("deletedoctortreatment/{id}")
    public String deleteDoctorTreatment(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "doctortreatment/deletedoctortreatment";
    }
}
