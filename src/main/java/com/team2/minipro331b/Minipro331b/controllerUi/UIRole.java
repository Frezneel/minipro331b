package com.team2.minipro331b.Minipro331b.controllerUi;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("role")
public class UIRole {

    @RequestMapping("")
    public String role(){
        return "roleHTML/role";
    }

    @RequestMapping("addrole")
    public String addRole() {
        return "roleHTML/addrole";
    }

    @RequestMapping("editrole/{id}")
    public String editRole(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "roleHTML/editrole";
    }

    @RequestMapping("deleterole/{id}")
    public String deleteRole(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "roleHTML/deleterole";
    }
}
