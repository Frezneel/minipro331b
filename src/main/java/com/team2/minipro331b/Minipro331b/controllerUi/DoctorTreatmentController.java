package com.team2.minipro331b.Minipro331b.controllerUi;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("doctortreatment")
public class DoctorTreatmentController {

    @RequestMapping("adddoctortreatment/{id}")
    public String addDoctorTreatment(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "doctortreatment/adddoctortreatment";
    }

    @RequestMapping("deletedoctortreatment/{id}")
    public String deleteDoctorTreatment(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "doctortreatment/deletedoctortreatment";
    }

}
