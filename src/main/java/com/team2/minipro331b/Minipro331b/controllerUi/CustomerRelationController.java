package com.team2.minipro331b.Minipro331b.controllerUi;

import com.team2.minipro331b.Minipro331b.model.M_Customer_Relation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "customerrelation")
public class CustomerRelationController {

    @RequestMapping("")
    public String customerrelation(){ return "customerrelation/customerrelation";}

    @RequestMapping("addcustomerrelation")
    public String addcustomerrelation() {return "customerrelation/addcustomerrelation"; }

    @RequestMapping("editcustomerrelation/{id}")
    public String editcustomerrelation(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "customerrelation/editcustomerrelation";
    }

    @RequestMapping("deletecustomerrelation/{id}")
    public String deletecustomerrelation(@PathVariable("id") Long id, Model model ) {
        model.addAttribute("id", id);
        return "customerrelation/deletecustomerrelation";
    }
}
