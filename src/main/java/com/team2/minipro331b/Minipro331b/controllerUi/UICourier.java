package com.team2.minipro331b.Minipro331b.controllerUi;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("courier")
public class UICourier {

    @RequestMapping("")
    public String courier(){
        return "courierHTML/courier";
    }

    @RequestMapping("addcourier")
    public String addCourier() {
        return "courierHTML/addcourier";
    }

    @RequestMapping("editcourier/{id}")
    public String editCourier(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "courierHTML/editcourier";
    }

    @RequestMapping("deletecourier/{id}")
    public String deleteCourier(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "courierHTML/deletecourier";
    }
}
