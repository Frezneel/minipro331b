package com.team2.minipro331b.Minipro331b.controllerUi;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "customer")
public class CustomerController {

//    @RequestMapping("customer_id={id}")
//    public String customerid(@PathVariable("id") Long id, Model model){
//        model.addAttribute("id", id);
//        return "customer/customer";}

    @RequestMapping("")
    public String customer(){ return "customer/customer";}

    @RequestMapping("addcustomer")
    public String addcustomer() {return "customer/addcustomer"; }

    @RequestMapping("editcustomer/{id}")
    public String editcustomer(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "customer/editcustomer";
    }

    @RequestMapping("deletecustomer/{id}")
    public String deletecustomer(@PathVariable("id") Long id, Model model ) {
        model.addAttribute("id", id);
        return "customer/deletecustomer";
    }
}
