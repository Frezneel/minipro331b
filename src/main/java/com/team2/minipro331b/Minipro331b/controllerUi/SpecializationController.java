package com.team2.minipro331b.Minipro331b.controllerUi;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "specialization")
public class SpecializationController {

    @RequestMapping("")
    public String specialization()
    {
        return "specialization/specialization";
    }

    @RequestMapping("/addspecialization")
    public String addSpecialization() {
        return "specialization/addspecialization";
    }

    @RequestMapping("/editspecialization/{id}")
    public String editSpecialization(@PathVariable("id") Long id, Model model)
    {
        model.addAttribute("id", id);
        return "specialization/editspecialization";
    }

    @RequestMapping("deletespecialization/{id}")
    public String deleteKelas(@PathVariable("id") Long id, Model model)
    {
        model.addAttribute("id", id);
        return "specialization/deletespecialization";
    }
}
