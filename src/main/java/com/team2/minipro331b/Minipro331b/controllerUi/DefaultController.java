package com.team2.minipro331b.Minipro331b.controllerUi;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DefaultController {

    @RequestMapping("")
    public String utama(){
        return "dashboard";
    }

    @RequestMapping("/login")
    public String login(){
        return "login/login";
    }

    @RequestMapping("/lupapassword")
    public String lupapassword(){
        return "lupapassword/lupapassword";
    }
    @RequestMapping("/register")
    public String register(){
        return "register/register";
    }

    @RequestMapping("/register/otp/{email}")
    public String registerOtp(@PathVariable("email")String email, Model model){
        model.addAttribute(email);
        return "register/otp-register";
    }
    @RequestMapping("/register/setPassword/{email}")
    public String registerPassword(@PathVariable("email") String email, Model model){
        model.addAttribute(email);
        return "register/set-password";
    }

    @RequestMapping("/register/setUserData/{email}&{password}")
    public String registerUserData(@PathVariable("email") String email, @PathVariable("password") String password, Model model){
        model.addAttribute(email);
        model.addAttribute(password);
        return "register/set-userdata";
    }

    @RequestMapping("/register/info-register")
    public String registerInfo(){
        return "register/info-register";
    }

    @RequestMapping("/lupapassword/otp/{email}&{idUser}")
    public String lupapasswordOtp(@PathVariable("email")String email,@PathVariable("idUser")Long idUser, Model model){
        model.addAttribute("email",email);
        model.addAttribute("idUser",idUser);
        return "lupapassword/otp-lupapassword";
    }
    @RequestMapping("/lupapassword/setPassword/{email}&{idUser}")
    public String lupapasswordSet(@PathVariable("email")String email, @PathVariable("idUser")Long idUser, Model model){
        model.addAttribute("email",email);
        model.addAttribute("idUser",idUser);
        return "lupapassword/set-lupapassword";
    }

    @RequestMapping("/lupapassword/info-lupapassword")
    public String lupapasswordInfo(){
        return "lupapassword/info-lupapassword";
    }

}
