package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.M_Doctor;
import com.team2.minipro331b.Minipro331b.model.M_Location;
import com.team2.minipro331b.Minipro331b.repository.RepoDoctor;
import com.team2.minipro331b.Minipro331b.repository.RepoLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestDoctor {

    @Autowired
    public RepoDoctor repoDoctor;

    @GetMapping("/doctor")
    public ResponseEntity<List<M_Doctor>> getAllDoctor()
    {
        try {
            List<M_Doctor> m_doctors= this.repoDoctor.findAllNotDeleted();
            return new ResponseEntity<>(m_doctors, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/doctor")
    public ResponseEntity<Object> saveDoctor(@RequestBody M_Doctor m_doctor)
    {
        try {
            m_doctor.setCreated_by(1L);
            m_doctor.setCreated_on(new Date());
            this.repoDoctor.save(m_doctor);
            return new ResponseEntity<>(m_doctor, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>("Failled", HttpStatus.BAD_REQUEST);
        }
    }
}
