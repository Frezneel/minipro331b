package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.M_Location;
import com.team2.minipro331b.Minipro331b.model.M_Medical_Facility;
import com.team2.minipro331b.Minipro331b.repository.RepoLocation;
import com.team2.minipro331b.Minipro331b.repository.RepoMedicalFacility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestMedicalFacility {

    @Autowired
    public RepoMedicalFacility repoMedicalFacility;

    @GetMapping("/medicalfacility")
    public ResponseEntity<List<M_Medical_Facility>> getAllMedicalFacility()
    {
        try {
            List<M_Medical_Facility> m_medical_facilities= this.repoMedicalFacility.findAllNotDeleted();
            return new ResponseEntity<>(m_medical_facilities, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/medicalfacility")
    public ResponseEntity<Object> saveMedicalFacility(@RequestBody M_Medical_Facility m_medical_facility)
    {
        try {
            m_medical_facility.setCreated_by(1L);
            m_medical_facility.setCreated_on(new Date());
            this.repoMedicalFacility.save(m_medical_facility);
            return new ResponseEntity<>(m_medical_facility, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>("Failled", HttpStatus.BAD_REQUEST);
        }
    }
}
