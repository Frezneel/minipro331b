package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.M_Blood_Group;
import com.team2.minipro331b.Minipro331b.repository.RepoBloodGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestBloodGroup {

    @Autowired
    private RepoBloodGroup repoBloodGroup;

    @GetMapping("/bloodgroup/getAllBloodGroup")
    public ResponseEntity<List<M_Blood_Group>> getAllBloodGroup() {
        try {
            List<M_Blood_Group> m_blood_group = this.repoBloodGroup.findAll();
            return new ResponseEntity<>(m_blood_group, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/bloodgroup")
    public ResponseEntity<Object> saveBloodGroup(@RequestBody M_Blood_Group m_blood_group ) {
        try {
            m_blood_group.setCreated_by(1L);
            m_blood_group.setCreated_on(new Date());
            this.repoBloodGroup.save(m_blood_group);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}

