package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.M_Customer;
import com.team2.minipro331b.Minipro331b.repository.RepoCustomer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestCustomer {

    @Autowired
    private RepoCustomer repoCustomer;

    @GetMapping("/customer")
    public ResponseEntity<List<M_Customer>> getAllCustomer(){
        try {
            List<M_Customer> m_customer = this.repoCustomer.findAllNotDeleted();
            return new ResponseEntity<>(m_customer, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/customer")
    public ResponseEntity<Object> SaveCustomer(@RequestBody M_Customer m_customer ){
        try {
            m_customer.setCreated_by(3L);
            m_customer.setCreated_on(new Date());
            this.repoCustomer.save(m_customer);
            return new ResponseEntity<>(m_customer, HttpStatus.OK);
        } catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/register/as/code=ROLE_PASIEN")
    public ResponseEntity<Object> SaveCustomerByCode(@RequestBody M_Customer m_customer ){
        try {
            m_customer.setCreated_by(3L);
            m_customer.setCreated_on(new Date());
            this.repoCustomer.save(m_customer);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        } catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/customer/{id}")
    public ResponseEntity<List<M_Customer>> getCustomerById(@PathVariable("id") Long id)
    {
        try {
            Optional<M_Customer> m_customer = this.repoCustomer.findById(id);
            if (m_customer.isPresent()) {
                ResponseEntity rest = new ResponseEntity<>(m_customer, HttpStatus.OK);
                return rest;
            }
            else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchcustomer={kata}")
    public ResponseEntity<List<M_Customer>> GetSearchCustomer(@PathVariable("kata") String kata)
    {
        try{
            List<M_Customer> m_customer = this.repoCustomer.search(kata);
            return new ResponseEntity<>(m_customer, HttpStatus.OK);
        }
        catch(Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/customer/{id}")
    public ResponseEntity<Object> UpdateCustomer(@RequestBody M_Customer m_customer, @PathVariable("id") Long id)
    {
        Optional<M_Customer> m_customerData= this.repoCustomer.findById(id);


        if (m_customerData.isPresent())
        {
            m_customer.setCreated_by(3L);
            m_customer.setCreated_on(new Date());
            m_customer.setModified_by(3L);
            m_customer.setModified_on(new Date());
            m_customer.setId(id);
            this.repoCustomer.save(m_customer);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    @DeleteMapping("/customer/{id}")
    public String DeleteCustomer(@PathVariable("id") Long id)
    {
        try {
            M_Customer m_customer  = this.repoCustomer.findByIdData(id);
            m_customer.setDeleted_by(3L);
            m_customer.setDeleted_on(new Date());
            m_customer.setIs_delete(true);
            this.repoCustomer.save(m_customer);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }
}
