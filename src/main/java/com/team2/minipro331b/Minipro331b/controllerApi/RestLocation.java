package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.M_Location;
import com.team2.minipro331b.Minipro331b.model.M_Specialization;
import com.team2.minipro331b.Minipro331b.repository.RepoLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestLocation {

    @Autowired
    public RepoLocation repoLocation;

    @GetMapping("/location")
    public ResponseEntity<List<M_Location>> getAllLocation()
    {
        try {
            List<M_Location> m_locations= this.repoLocation.findAllNotDeleted();
            return new ResponseEntity<>(m_locations, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/location")
    public ResponseEntity<Object> saveLocation(@RequestBody M_Location m_location)
    {
        try {
            m_location.setCreated_by(1L);
            m_location.setCreated_on(new Date());
            this.repoLocation.save(m_location);
            return new ResponseEntity<>(m_location, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>("Failled", HttpStatus.BAD_REQUEST);
        }
    }
}
