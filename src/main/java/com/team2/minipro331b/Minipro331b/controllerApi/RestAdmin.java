package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.M_Admin;
import com.team2.minipro331b.Minipro331b.repository.RepoAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")

public class RestAdmin {

    @Autowired
    private RepoAdmin repoAdmin;

    @GetMapping("/admin/getAllAdmin")
    public ResponseEntity<List<M_Admin>> getAllAdmin(){
        try {
            List<M_Admin> m_admin = this.repoAdmin.findAll();
            return new ResponseEntity<>(m_admin, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/admin")
    public ResponseEntity<Object> saveAdmin(@RequestBody M_Admin m_admin){
        try {
            m_admin.setCreated_by(1L);
            m_admin.setCreated_on(new Date());
            this.repoAdmin.save(m_admin);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

}
