package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.M_Customer_Member;
import com.team2.minipro331b.Minipro331b.model.T_Customer_Chat;
import com.team2.minipro331b.Minipro331b.repository.RepoCustomerChat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestCustomerChat {

    @Autowired
    private RepoCustomerChat repoCustomerChat;

    @GetMapping("/customerchat/getAllCustomerChat")
    public ResponseEntity<List<T_Customer_Chat>> getAllCustomerChat() {
        try {
            List<T_Customer_Chat> t_customer_chat = this.repoCustomerChat.findAll();
            return new ResponseEntity<>(t_customer_chat, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/customerchat")
    public ResponseEntity<Object> saveCustomerChat(@RequestBody T_Customer_Chat t_customer_chat ) {
        try {
            t_customer_chat.setCreated_by(3L);
            t_customer_chat.setCreated_on(new Date());
            this.repoCustomerChat.save(t_customer_chat);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

}
