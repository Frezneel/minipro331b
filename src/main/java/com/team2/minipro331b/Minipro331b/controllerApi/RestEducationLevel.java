package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.M_Education_Level;
import com.team2.minipro331b.Minipro331b.model.M_Location;
import com.team2.minipro331b.Minipro331b.repository.RepoEducationLevel;
import com.team2.minipro331b.Minipro331b.repository.RepoLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestEducationLevel {

    @Autowired
    public RepoEducationLevel repoEducationLevel;

    @GetMapping("/educationlevel")
    public ResponseEntity<List<M_Education_Level>> getAllEducationLevel()
    {
        try {
            List<M_Education_Level> m_education_levels= this.repoEducationLevel.findAllNotDeleted();
            return new ResponseEntity<>(m_education_levels, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/educationlevel")
    public ResponseEntity<Object> saveEducationLevel(@RequestBody M_Education_Level m_education_level)
    {
        try {
            m_education_level.setCreated_by(1L);
            m_education_level.setCreated_on(new Date());
            this.repoEducationLevel.save(m_education_level);
            return new ResponseEntity<>(m_education_level, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>("Failled", HttpStatus.BAD_REQUEST);
        }
    }
}
