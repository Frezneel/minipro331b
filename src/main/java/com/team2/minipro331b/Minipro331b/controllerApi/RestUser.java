package com.team2.minipro331b.Minipro331b.controllerApi;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.team2.minipro331b.Minipro331b.model.M_User;
import com.team2.minipro331b.Minipro331b.repository.RepoUser;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestUser {

    @Autowired
    private RepoUser repoUser;

    @GetMapping("/user/session")
    public Map isSessionLogin(HttpSession httpSession){
       Long user_id = (Long) httpSession.getAttribute("userID");
       Long role_id = (Long) httpSession.getAttribute("roleID");

       if (user_id != null){
           Map obj = new HashMap();
           obj.put("user_id", user_id);
           obj.put("role_id", role_id);
           List data =  new ArrayList<>();
           data.add(obj);
           return obj;
       }else {
           Map obj = new HashMap();
           obj.put("user_id", 0);
           obj.put("role_id", 0);
           List data =  new ArrayList<>();
           data.add(obj);
           return obj;
       }
    }

    @GetMapping("/user/getAllUser")
    public ResponseEntity<List<M_User>> getAllUser(){
        try {
            List<M_User> m_user = this.repoUser.findAll();
            return new ResponseEntity<>(m_user, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/user/userID={user_id}")
    public ResponseEntity<Object> getUserById(@PathVariable("user_id") Long id){
        try {
            Optional<M_User> m_user = this.repoUser.findById(id);
            if (m_user.isPresent()) {
                return new ResponseEntity<>(m_user, HttpStatus.OK);
            } else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("user/email={email}")
    public boolean isEmailTerdaftar(@PathVariable("email") String email){
        Optional<M_User> m_user = this.repoUser.isEmailTerdaftar(email);
        if (m_user.isPresent()){
            return true;
        }else {
            return false;
        }
    }

    @PostMapping("/user/callResponse")
    public ResponseEntity<Object> saveUser(@RequestBody M_User m_user){
        try {
            m_user.setCreated_by(1L);
            m_user.setCreated_on(new Date());
            this.repoUser.save(m_user);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/user/login")
    public String sessionLogin(@RequestParam String email, @RequestParam String password, HttpSession session){
        M_User userLogin = this.repoUser.login(email, password);
        if (userLogin != null){
            Boolean islocked = userLogin.getIs_locked() == null ? false : userLogin.getIs_locked();
            if(!islocked){
                session.setAttribute("userID", userLogin.getId());
                session.setAttribute("roleID", userLogin.getRole_id());
                userLogin.setModified_by(userLogin.getId());
                userLogin.setModified_on(new Date());
                userLogin.setLast_login(new Date());
                this.repoUser.save(userLogin);
                return "sukses";
            }else {
                return "terkunci";
            }
        }else {
            Optional<M_User> cekEmail = this.repoUser.isEmailTerdaftar(email);
            if (cekEmail.isPresent()){
                Integer loginAttempt = cekEmail.get().getLogin_attempt() == null ? 1 : cekEmail.get().getLogin_attempt();
                if (loginAttempt >= 3){
                    M_User userLock = cekEmail.get();
                    userLock.setIs_locked(true);
                    this.repoUser.save(userLock);
                }else {
                    loginAttempt += 1;
                    cekEmail.get().setLogin_attempt(loginAttempt);
                    this.repoUser.save(cekEmail.get());
                }
                return "gagal";
            }else {
                return "no-email";
            }
        }
    }

    @PostMapping("/user/logout")
    public String seesionLogout(HttpSession session){
        session.invalidate();
        return "sukses";
    }
}
