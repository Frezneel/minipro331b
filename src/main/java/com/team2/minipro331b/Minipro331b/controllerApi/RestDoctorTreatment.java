package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.T_Doctor_Treatment;
import com.team2.minipro331b.Minipro331b.repository.RepoDoctorTreatment;
import com.team2.minipro331b.Minipro331b.repository.RepoLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestDoctorTreatment {

    @Autowired
    public RepoDoctorTreatment repoDoctorTreatment;

    @GetMapping("/doctortreatment")
    public ResponseEntity<List<T_Doctor_Treatment>> getAllDoctorTreatment()
    {
        try {
            List<T_Doctor_Treatment> t_doctor_treatments = this.repoDoctorTreatment.findAllNotDeleted();
            return new ResponseEntity<>(t_doctor_treatments, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/doctortreatment")
    public ResponseEntity<Object> saveDoctorTreatment(@RequestBody T_Doctor_Treatment t_doctor_treatment)
    {
        try {
            t_doctor_treatment.setCreated_by(1L);
            t_doctor_treatment.setCreated_on(new Date());
            this.repoDoctorTreatment.save(t_doctor_treatment);
            return new ResponseEntity<>(t_doctor_treatment, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>("Failled", HttpStatus.BAD_REQUEST);
        }
    }
}
