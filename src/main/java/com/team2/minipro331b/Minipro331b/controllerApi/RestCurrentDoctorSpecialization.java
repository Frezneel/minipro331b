package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.T_Current_Doctor_Specialization;
import com.team2.minipro331b.Minipro331b.model.T_Doctor_Office;
import com.team2.minipro331b.Minipro331b.repository.RepoCurrentDoctorSpecialization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestCurrentDoctorSpecialization {

    @Autowired
    private RepoCurrentDoctorSpecialization repoCurrentDoctorSpecialization;

    @GetMapping("/currentdoctorspecialization/getAllCurrentDoctorSpecialization")
    public ResponseEntity<List<T_Current_Doctor_Specialization>> getAllCurrentDoctorSpecialization(){
        try {
            List<T_Current_Doctor_Specialization> t_current_doctor_specializations = this.repoCurrentDoctorSpecialization.findAll();
            return new ResponseEntity<>(t_current_doctor_specializations, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/currentdoctorspecialization/")
    public ResponseEntity<Object> saveCurrentDoctorSpecialization(@RequestBody T_Current_Doctor_Specialization t_current_doctor_specialization){
        try {
            t_current_doctor_specialization.setCreated_by(1L);
            t_current_doctor_specialization.setCreated_on(new Date());
            this.repoCurrentDoctorSpecialization.save(t_current_doctor_specialization);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/currentdoctorspecialization/doctorID={doctor_id}")
    public ResponseEntity<Object> getSpecializationById(@PathVariable("doctor_id") Long id){
        try {
            Optional<T_Current_Doctor_Specialization> doctor = this.repoCurrentDoctorSpecialization.getSpecializationById(id);
            if (doctor.isPresent()) {
                return new ResponseEntity<>(doctor, HttpStatus.OK);
            } else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

}
