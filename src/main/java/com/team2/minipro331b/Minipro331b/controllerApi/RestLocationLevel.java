package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.M_Level_Location;
import com.team2.minipro331b.Minipro331b.model.M_Location;
import com.team2.minipro331b.Minipro331b.repository.RepoLocation;
import com.team2.minipro331b.Minipro331b.repository.RepoLocationLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestLocationLevel {

    @Autowired
    public RepoLocationLevel repoLocationLevel;

    @GetMapping("/locationlevel")
    public ResponseEntity<List<M_Level_Location>> getAllLocationLevel()
    {
        try {
            List<M_Level_Location> m_level_locations= this.repoLocationLevel.findAllNotDeleted();
            return new ResponseEntity<>(m_level_locations, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/locationlevel")
    public ResponseEntity<Object> saveLocationLevel(@RequestBody M_Level_Location m_level_location)
    {
        try {
            m_level_location.setCreated_by(1L);
            m_level_location.setCreated_on(new Date());
            this.repoLocationLevel.save(m_level_location);
            return new ResponseEntity<>(m_level_location, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>("Failled", HttpStatus.BAD_REQUEST);
        }
    }
}
