package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.MailStructure;
import com.team2.minipro331b.Minipro331b.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class RestEmail {
    @Autowired
    private EmailService emailService;

    @PostMapping("/mail/send/")
    public String sendMailOTP(@RequestParam String mailTo, @RequestBody MailStructure mailStructure){
        try {
            emailService.sendOTP(mailTo, mailStructure);
            return "Berhasil";
        }catch (Exception exception){
            return "Gagal";
        }
    }

}
