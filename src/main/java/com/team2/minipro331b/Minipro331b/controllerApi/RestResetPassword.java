package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.T_Reset_Password;
import com.team2.minipro331b.Minipro331b.repository.RepoResetPassword;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestResetPassword {

    @Autowired
    private RepoResetPassword repoResetPassword;

        @GetMapping("/resetpassword/validasiPassword/{idUser}")
    public ResponseEntity<List<T_Reset_Password>> validasiPassword(@PathVariable("idUser") Long idUser){
        try {
            List<T_Reset_Password> t_reset_passwords = this.repoResetPassword.listValidasiPasswordByIdBiodata(idUser);
            if (t_reset_passwords.isEmpty()){
                return new ResponseEntity<>(t_reset_passwords, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(t_reset_passwords, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/resetpassword/saveResetPassword")
    public ResponseEntity<Object> saveResetPassword(@RequestBody T_Reset_Password t_reset_password){
        try {
            t_reset_password.setCreated_on(new Date());
            this.repoResetPassword.save(t_reset_password);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
