package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.M_Biodata;
import com.team2.minipro331b.Minipro331b.model.M_User;
import com.team2.minipro331b.Minipro331b.repository.RepoBiodata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestBiodata {

    @Autowired
    private RepoBiodata repoBiodata;

    @GetMapping("/biodata/getAllBiodata")
    public ResponseEntity<List<M_Biodata>> getAllBiodata(){
        try {
            List<M_Biodata> m_biodata = this.repoBiodata.findAll();
            return new ResponseEntity<>(m_biodata, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/biodata/biodataID={biodata_id}")
    public ResponseEntity<Object> getUserById(@PathVariable("biodata_id") Long id){
        try {
            Optional<M_Biodata> biodata = this.repoBiodata.getBiodataById(id);
            if (biodata.isPresent()) {
                return new ResponseEntity<>(biodata, HttpStatus.OK);
            } else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/biodata")
    public ResponseEntity<Object> saveBiodata(@RequestBody M_Biodata m_biodata){
        try {
            m_biodata.setCreated_by(1L);
            m_biodata.setCreated_on(new Date());
            this.repoBiodata.save(m_biodata);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/biodata/callId")
    public ResponseEntity<Long> saveBiodataCallId (@RequestBody M_Biodata m_biodata){
        try {
            m_biodata.setCreated_by(1L);
            m_biodata.setCreated_on(new Date());
            this.repoBiodata.save(m_biodata);
            return new ResponseEntity<>(m_biodata.getId(), HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

}
