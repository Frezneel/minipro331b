package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.M_User;
import com.team2.minipro331b.Minipro331b.model.T_Token;
import com.team2.minipro331b.Minipro331b.repository.RepoToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestToken {

    @Autowired
    private RepoToken repoToken;

    @GetMapping("/token/getAllToken")
    public ResponseEntity<List<T_Token>> getAllUser(){
        try {
            List<T_Token> t_token = this.repoToken.findAll();
            return new ResponseEntity<>(t_token, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/token/lastToken/")
    public String getLastToken(@RequestParam String email, @RequestParam String used_for, @RequestParam String input){
        try {
            Optional<T_Token> t_token = this.repoToken.getLastToken(email, used_for, input);
            if (t_token.isPresent()){
                Long expired = t_token.get().getExpired_on().getTime() - new Date().getTime();
                Boolean is_expired = t_token.get().getIs_expired();
                if (expired > 0 && !is_expired){
                    return "aman";
                }else {
                    return "kadaluarsa";
                }
            }else {
                return "salah";
            }
        }catch (Exception exception){
            return exception.getMessage();
        }
    }

    @PostMapping("/token/save")
    public ResponseEntity<Object> saveToken(@RequestBody T_Token t_token){
        try {
            Date expiredDate = new Date();
            Integer minutes = expiredDate.getMinutes();
            expiredDate.setMinutes(minutes + 3);

            t_token.setCreated_by(1L);
            t_token.setCreated_on(new Date());
            t_token.setExpired_on(expiredDate);
            this.repoToken.save(t_token);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/token/tokenToExpired/")
    public ResponseEntity<Object> tokenToExpired(@RequestParam String email, @RequestParam String used_for){
        try {
            T_Token t_token = this.repoToken.getLastToken(email, used_for);
            t_token.setIs_expired(true);
            this.repoToken.save(t_token);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
