package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.M_Location;
import com.team2.minipro331b.Minipro331b.model.T_Doctor_Office;
import com.team2.minipro331b.Minipro331b.repository.RepoDoctorOffice;
import com.team2.minipro331b.Minipro331b.repository.RepoLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestDoctorOffice {

    @Autowired
    public RepoDoctorOffice repoDoctorOffice;

    @GetMapping("/doctoroffice")
    public ResponseEntity<List<T_Doctor_Office>> getAllDoctorOffice()
    {
        try {
            List<T_Doctor_Office> t_doctor_offices= this.repoDoctorOffice.findAllNotDeleted();
            return new ResponseEntity<>(t_doctor_offices, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/doctoroffice")
    public ResponseEntity<Object> saveDoctorOffice(@RequestBody T_Doctor_Office t_doctor_office)
    {
        try {
            t_doctor_office.setCreated_by(1L);
            t_doctor_office.setCreated_on(new Date());
            this.repoDoctorOffice.save(t_doctor_office);
            return new ResponseEntity<>(t_doctor_office, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>("Failled", HttpStatus.BAD_REQUEST);
        }
    }
}
