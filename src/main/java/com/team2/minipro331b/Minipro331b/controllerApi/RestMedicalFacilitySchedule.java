package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.M_Medical_Facility_Schedule;
import com.team2.minipro331b.Minipro331b.repository.RepoMedicalFacilitySchedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestMedicalFacilitySchedule {
    @Autowired
    private RepoMedicalFacilitySchedule repoMedicalFacilitySchedule;

    @GetMapping("/medicalfacilityschedule/getAllMedicalFacilitySchedule")
    public ResponseEntity<List<M_Medical_Facility_Schedule>> getAllMedicalFacilitySchedule() {
        try {
            List<M_Medical_Facility_Schedule> m_medical_facility_schedule = this.repoMedicalFacilitySchedule.findAll();
            return new ResponseEntity<>(m_medical_facility_schedule, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/medicalfacilityschedule")
    public ResponseEntity<Object> saveMedicalFacilitySchedule(@RequestBody M_Medical_Facility_Schedule m_medical_facility_schedule ) {
        try {
            m_medical_facility_schedule.setCreated_by(1L);
            m_medical_facility_schedule.setCreated_on(new Date());
            this.repoMedicalFacilitySchedule.save(m_medical_facility_schedule);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
