package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.M_Doctor_Education;
import com.team2.minipro331b.Minipro331b.model.M_Location;
import com.team2.minipro331b.Minipro331b.model.T_Doctor_Office;
import com.team2.minipro331b.Minipro331b.repository.RepoDoctorEducation;
import com.team2.minipro331b.Minipro331b.repository.RepoLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestDoctorEducation {

    @Autowired
    public RepoDoctorEducation repoDoctorEducation;

    @GetMapping("/doctoreducation")
    public ResponseEntity<List<M_Doctor_Education>> getAllDoctorEducation()
    {
        try {
            List<M_Doctor_Education> m_doctor_educations= this.repoDoctorEducation.findAllNotDeleted();
            return new ResponseEntity<>(m_doctor_educations, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/doctoreducation/")
    public ResponseEntity<List<M_Doctor_Education>> getAllOfficeByIdEducation(@RequestParam Long doctor_id){
        try {
            List<M_Doctor_Education> m_doctor_education = this.repoDoctorEducation.findAllByIdEducation(doctor_id);
            return new ResponseEntity<>(m_doctor_education, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/doctoreducation")
    public ResponseEntity<Object> saveDoctorEducation(@RequestBody M_Doctor_Education m_doctor_education)
    {
        try {
            m_doctor_education.setCreated_by(1L);
            m_doctor_education.setCreated_on(new Date());
            this.repoDoctorEducation.save(m_doctor_education);
            return new ResponseEntity<>(m_doctor_education, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>("Failled", HttpStatus.BAD_REQUEST);
        }
    }
}
