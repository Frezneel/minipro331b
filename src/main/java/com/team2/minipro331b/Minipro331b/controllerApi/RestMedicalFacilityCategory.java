package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.M_Location;
import com.team2.minipro331b.Minipro331b.model.M_Medical_Facility_Category;
import com.team2.minipro331b.Minipro331b.repository.RepoLocation;
import com.team2.minipro331b.Minipro331b.repository.RepoMedicalFacilityCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestMedicalFacilityCategory {

    @Autowired
    public RepoMedicalFacilityCategory repoMedicalFacilityCategory;

    @GetMapping("/medicalfacilitycategory")
    public ResponseEntity<List<M_Medical_Facility_Category>> getAllMedicalFacilityCategory()
    {
        try {
            List<M_Medical_Facility_Category> m_medical_facility_categories= this.repoMedicalFacilityCategory.findAllNotDeleted();
            return new ResponseEntity<>(m_medical_facility_categories, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/medicalfacilitycategory")
    public ResponseEntity<Object> saveMedicalFacilityCategory(@RequestBody M_Medical_Facility_Category m_medical_facility_category)
    {
        try {
            m_medical_facility_category.setCreated_by(1L);
            m_medical_facility_category.setCreated_on(new Date());
            this.repoMedicalFacilityCategory.save(m_medical_facility_category);
            return new ResponseEntity<>(m_medical_facility_category, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>("Failled", HttpStatus.BAD_REQUEST);
        }
    }
}
