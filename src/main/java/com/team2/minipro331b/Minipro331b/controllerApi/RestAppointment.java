package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.T_Appointment;
import com.team2.minipro331b.Minipro331b.model.T_Customer_Chat;
import com.team2.minipro331b.Minipro331b.repository.RepoAppointment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestAppointment {

    @Autowired
    private RepoAppointment repoAppointment;

    @GetMapping("/appointment/getAllAppointment")
    public ResponseEntity<List<T_Appointment>> getAllAppointment() {
        try {
            List<T_Appointment> t_appointment = this.repoAppointment.findAll();
            return new ResponseEntity<>(t_appointment, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/appointment")
    public ResponseEntity<Object> saveAppointment(@RequestBody T_Appointment t_appointment ) {
        try {
            t_appointment.setCreated_by(3L);
            t_appointment.setCreated_on(new Date());
            this.repoAppointment.save(t_appointment);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
