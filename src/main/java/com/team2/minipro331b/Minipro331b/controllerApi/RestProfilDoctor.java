package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.M_Doctor;
import com.team2.minipro331b.Minipro331b.model.M_Specialization;
import com.team2.minipro331b.Minipro331b.model.ProfilDoctor;
import com.team2.minipro331b.Minipro331b.repository.RepoDoctor;
import com.team2.minipro331b.Minipro331b.repository.RepoProfilDoctor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestProfilDoctor {

    @Autowired
    public RepoProfilDoctor repoProfilDoctor;

    @GetMapping("/profildoctor")
    public ResponseEntity<List<ProfilDoctor>> getAllProfilDoctor()
    {
        try {
            List<ProfilDoctor> profilDoctors= this.repoProfilDoctor.findAll();
            return new ResponseEntity<>(profilDoctors, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/profildoctor")
    public ResponseEntity<Object> saveProfilDoctor(@RequestBody ProfilDoctor profilDoctor)
    {
        try {
            profilDoctor.setCreated_by(1L);
            profilDoctor.setCreated_on(new Date());
            this.repoProfilDoctor.save(profilDoctor);
            return new ResponseEntity<>(profilDoctor, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>("Failled", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/profildoctor/{id}")
    public ResponseEntity<List<ProfilDoctor>> getProfilDoctorById(@PathVariable("id") Long id)
    {
        try {
            Optional<ProfilDoctor> profilDoctor = this.repoProfilDoctor.findById(id);

            if (profilDoctor.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(profilDoctor, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/profildoctor/{id}")
    public ResponseEntity<Object> updateProfilDoctor(@RequestBody ProfilDoctor profilDoctor, @PathVariable("id") Long id)
    {
        Optional<ProfilDoctor> profilDoctorData = this.repoProfilDoctor.findById(id);

        if (profilDoctorData.isPresent()) {
            ProfilDoctor existingProfilDoctor = profilDoctorData.get();

            Date createOn = existingProfilDoctor.getCreated_on();
            Long createdBy = existingProfilDoctor.getCreated_by();

            if (createOn != null && createdBy != null) {
                // Set only fields that are meant to be updated
                existingProfilDoctor.setId(profilDoctor.getId());

                existingProfilDoctor.setModified_by(1L);
                existingProfilDoctor.setModified_on(new Date());

                this.repoProfilDoctor.save(existingProfilDoctor);
                ResponseEntity<Object> response = new ResponseEntity<>("Success", HttpStatus.OK);
                return response;
            } else {
                return ResponseEntity.badRequest().body("Nilai createOn atau createdBy kosong");
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
