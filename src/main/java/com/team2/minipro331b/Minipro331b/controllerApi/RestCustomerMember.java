package com.team2.minipro331b.Minipro331b.controllerApi;


import com.team2.minipro331b.Minipro331b.model.M_Customer_Member;
import com.team2.minipro331b.Minipro331b.repository.RepoCustomerMember;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestCustomerMember {

    @Autowired
    private RepoCustomerMember repoCustomerMember;

    @GetMapping("/customermember/getAllCustomerMember")
    public ResponseEntity<List<M_Customer_Member>> getAllCustomerMember() {
        try {
            List<M_Customer_Member> m_customer_member = this.repoCustomerMember.findAll();
            return new ResponseEntity<>(m_customer_member, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/customermember")
    public ResponseEntity<Object> saveCustomerMember(@RequestBody M_Customer_Member m_customer_member ) {
        try {
            m_customer_member.setCreated_by(1L);
            m_customer_member.setCreated_on(new Date());
            this.repoCustomerMember.save(m_customer_member);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

}
