package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.M_Location;
import com.team2.minipro331b.Minipro331b.model.T_Doctor_Office_Treatment;
import com.team2.minipro331b.Minipro331b.repository.RepoDoctorOfficeTreatment;
import com.team2.minipro331b.Minipro331b.repository.RepoLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestDoctorOfficeTreatment {

    @Autowired
    public RepoDoctorOfficeTreatment repoDoctorOfficeTreatment;

    @GetMapping("/doctorofficetreatment")
    public ResponseEntity<List<T_Doctor_Office_Treatment>> getAllDoctorOfficeTreatment()
    {
        try {
            List<T_Doctor_Office_Treatment> t_doctor_office_treatments= this.repoDoctorOfficeTreatment.findAllNotDeleted();
            return new ResponseEntity<>(t_doctor_office_treatments, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/doctorofficetreatment")
    public ResponseEntity<Object> saveDoctorOfficeTreatment(@RequestBody T_Doctor_Office_Treatment t_doctor_office_treatment)
    {
        try {
            t_doctor_office_treatment.setCreated_by(1L);
            t_doctor_office_treatment.setCreated_on(new Date());
            this.repoDoctorOfficeTreatment.save(t_doctor_office_treatment);
            return new ResponseEntity<>(t_doctor_office_treatment, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>("Failled", HttpStatus.BAD_REQUEST);
        }
    }
}
