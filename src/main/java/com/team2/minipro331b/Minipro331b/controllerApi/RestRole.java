package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.M_Role;
import com.team2.minipro331b.Minipro331b.repository.RepoRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;


@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestRole {

    @Autowired
    private RepoRole repoRole;
    @GetMapping("/getallrole")
    public ResponseEntity<List<M_Role>> GetAllRole(){
        try{
            List<M_Role> role = this.repoRole.findAllNotDeleted();
            return new ResponseEntity<>(role, HttpStatus.OK);
        }
        catch(Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/role/mapped")
    public ResponseEntity<Map<String, Object>> getAllPage(@RequestParam(defaultValue = "0")int halaman /*initial halaman pertama(sama kayak array)*/,
                                                          @RequestParam(defaultValue = "5") int ukuranHal /*initial jumlah data yang ditampilkan*/)
    {
        try{
            List<M_Role> role = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(halaman,ukuranHal);
            Page<M_Role> pageTuts;

            pageTuts = this.repoRole.findAllOrderByAsc(pagingSort);
            role = pageTuts.getContent();

            Map<String, Object> respon =  new HashMap<>();
            respon.put("role", role);
            respon.put("halamanSekarang", pageTuts.getNumber());
            respon.put("totalBarang", pageTuts.getTotalElements()); //total data ada berapa
            respon.put("totalHalaman", pageTuts.getTotalPages());

            return new ResponseEntity<>(respon, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/getrole/{id}")
    public ResponseEntity<List<M_Role>> GetRoleById(@PathVariable("id") Long id)
    {
        try{
            Optional<M_Role> role = this.repoRole.findById(id);

            if(role.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(role, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchrole={kata}")
    public ResponseEntity<List<M_Role>> GetSearchRole(@PathVariable("kata") String kata)
    {
        try{
            List<M_Role> role = this.repoRole.search(kata);
            return new ResponseEntity<>(role, HttpStatus.OK);
        }
        catch(Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/role/search")
    public ResponseEntity<Map<String, Object>> getSearchMapped(@RequestParam(defaultValue = "0") int halaman,
                                                               @RequestParam(defaultValue = "5") int ukuranHal,
                                                               @RequestParam(defaultValue = "") String kata){
        try {
            List<M_Role> role = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(halaman, ukuranHal, Sort.by("name").ascending());
            Page<M_Role> pageTuts;

            pageTuts = this.repoRole.searchMapped(pagingSort, kata);
            role = pageTuts.getContent();

            Map<String, Object> respon = new HashMap<>();
            respon.put("role", role);
            respon.put("halamanSekarang", pageTuts.getNumber());
            respon.put("totalBarang", pageTuts.getTotalElements());
            respon.put("totalHalaman", pageTuts.getTotalPages());

            return new ResponseEntity<>(respon, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/role/sort")
    public ResponseEntity<Map<String, Object>> getSortMapped(@RequestParam(defaultValue = "0") int halaman,
                                                             @RequestParam(defaultValue = "5") int ukuranHal,
                                                             @RequestParam String kata,
                                                             @RequestParam(defaultValue = "true") Boolean isAsc){
        try {
            List<M_Role> role = new ArrayList<>();
            Pageable pagingSort;
            if (isAsc){
                pagingSort = PageRequest.of(halaman, ukuranHal, Sort.by(kata).ascending());
            }else{
                pagingSort = PageRequest.of(halaman, ukuranHal, Sort.by(kata).descending());
            }
            Page<M_Role> pageTuts;
            pageTuts = this.repoRole.findAllOrder(pagingSort);
            role = pageTuts.getContent();

            Map<String, Object> respon = new HashMap<>();
            respon.put("role", role);
            respon.put("halamanSekarang", pageTuts.getNumber());
            respon.put("totalBarang", pageTuts.getTotalElements());
            respon.put("totalHalaman", pageTuts.getTotalPages());
            return new ResponseEntity<>(respon, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/addrole")
    public ResponseEntity<Object> SaveRole(@RequestBody M_Role role){
        try
        {
            role.setCreated_by(4L);
            role.setCreated_on(new Date());
            this.repoRole.save(role);
            return new ResponseEntity<>(role, HttpStatus.OK);
        }
        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/editrole/{id}")
    public ResponseEntity<Object> UpdateRole(@RequestBody M_Role role, @PathVariable("id") Long id)
    {
        Optional<M_Role> roleData = this.repoRole.findById(id);

        if (roleData.isPresent())
        {
            role.setId(id);
            role.setModified_by(1L);
            role.setModified_on(new Date());
            this.repoRole.save(role);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/deleterole/{id}") // jadikan delete agar tidak hapus permanen
    public ResponseEntity<Object> DeleteRole(@PathVariable("id") Long id)
    {
        M_Role role = this.repoRole.findByIdData(id);
        role.setDeleted_by(1L);
        role.setDeleted_on(new Date());
        role.setCreated_on(new Date());
        role.setIs_delete(true);
        this.repoRole.save(role);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

}
