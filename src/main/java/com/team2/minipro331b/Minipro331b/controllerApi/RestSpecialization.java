package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.controllerUi.SpecializationController;
import com.team2.minipro331b.Minipro331b.model.M_Specialization;
import com.team2.minipro331b.Minipro331b.repository.RepoSpecialization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestSpecialization {
    @Autowired
    public RepoSpecialization repoSpecialization;

    @GetMapping("/specialization")
    public ResponseEntity<List<M_Specialization>> getAllSpecialization()
    {
        try {
            List<M_Specialization> m_specializations = this.repoSpecialization.findAllNotDeleted();
            return new ResponseEntity<>(m_specializations, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/specialization")
    public ResponseEntity<Object> saveSpecialization(@RequestBody M_Specialization m_specialization)
    {
        try {
            m_specialization.setCreated_by(1L);
            m_specialization.setCreated_on(new Date());
            this.repoSpecialization.save(m_specialization);
            return new ResponseEntity<>(m_specialization, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>("Failled", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/specialization/{id}")
    public ResponseEntity<List<M_Specialization>> getSpecializationById(@PathVariable("id") Long id)
    {
        try {
            Optional<M_Specialization> m_specialization = this.repoSpecialization.findById(id);

            if (m_specialization.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(m_specialization, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/specialization/{id}")
    public ResponseEntity<Object> updateSpecialization(@RequestBody M_Specialization m_specialization, @PathVariable("id") Long id)
    {
        Optional<M_Specialization> m_specializationData = this.repoSpecialization.findById(id);
        m_specialization.setCreated_by(1L);
        m_specialization.setCreated_on(new Date());
        m_specialization.setModified_by(1L);
        m_specialization.setModified_on(new Date());

        if (m_specializationData.isPresent())
        {
            m_specialization.setId(id);
            this.repoSpecialization.save(m_specialization);
            ResponseEntity rest = new ResponseEntity("Success", HttpStatus.OK);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }

    }

    @DeleteMapping("/specialization/{id}")
    public String DeleteCustomerRelation(@PathVariable("id") Long id)
    {
        try {
            M_Specialization m_specialization  = this.repoSpecialization.findByIdData(id);
            m_specialization.setDeleted_by(1L);
            m_specialization.setDeleted_on(new Date());
            m_specialization.setIs_delete(true);
            this.repoSpecialization.save(m_specialization);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }

    @GetMapping("/searchspecialization={keyword}")
    public ResponseEntity<List<M_Specialization>> searchSpecialization(@PathVariable("keyword") String keyword)
    {
        try {
            List<M_Specialization> specializations = this.repoSpecialization.SearchSpecialization(keyword);
            return new ResponseEntity<>(specializations, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/specializationmapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<M_Specialization> specializations = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<M_Specialization> pageTuts;

            pageTuts = this.repoSpecialization.findAllNotDeleted(pagingSort);

            specializations = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("specialization", specializations);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
