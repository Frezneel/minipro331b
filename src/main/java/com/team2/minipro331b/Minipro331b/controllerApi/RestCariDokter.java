package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.Cari_Dokter;
import com.team2.minipro331b.Minipro331b.model.M_Doctor;
import com.team2.minipro331b.Minipro331b.repository.RepoCariDokter;
import com.team2.minipro331b.Minipro331b.repository.RepoDoctor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestCariDokter {

    @Autowired
    public RepoCariDokter repoCariDokter;

    @GetMapping("/caridokter")
    public ResponseEntity<List<Cari_Dokter>> getAllCariDokter()
    {
        try {
            List<Cari_Dokter> cari_dokters= this.repoCariDokter.findAllNotDeleted();
            return new ResponseEntity<>(cari_dokters, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
