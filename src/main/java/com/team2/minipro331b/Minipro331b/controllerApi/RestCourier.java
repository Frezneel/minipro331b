package com.team2.minipro331b.Minipro331b.controllerApi;

import com.team2.minipro331b.Minipro331b.model.M_Courier;
import com.team2.minipro331b.Minipro331b.repository.RepoCourier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;


@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestCourier {

    @Autowired
    private RepoCourier repoCourier;
    @GetMapping("/getallcourier")
    public ResponseEntity<List<M_Courier>> GetAllCourier(){
        try{
            List<M_Courier> courier = this.repoCourier.findAllNotDeleted();
            return new ResponseEntity<>(courier, HttpStatus.OK);
        }
        catch(Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/getcourier/{id}")
    public ResponseEntity<List<M_Courier>> GetCourierById(@PathVariable("id") Long id)
    {
        try{
            Optional<M_Courier> courier = this.repoCourier.findById(id);

            if(courier.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(courier, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/courier/mapped")
    public ResponseEntity<Map<String, Object>> getAllPage(@RequestParam(defaultValue = "0")int halaman /*initial halaman pertama(sama kayak array)*/,
                                                          @RequestParam(defaultValue = "5") int ukuranHal /*initial jumlah data yang ditampilkan*/)
    {
        try{
            List<M_Courier> courier = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(halaman,ukuranHal);
            Page<M_Courier> pageTuts;

            pageTuts = this.repoCourier.findAllOrderByAsc(pagingSort);
            courier = pageTuts.getContent();

            Map<String, Object> respon =  new HashMap<>();
            respon.put("courier", courier);
            respon.put("halamanSekarang", pageTuts.getNumber());
            respon.put("totalBarang", pageTuts.getTotalElements()); //total data ada berapa
            respon.put("totalHalaman", pageTuts.getTotalPages());

            return new ResponseEntity<>(respon, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchcourier={kata}")
    public ResponseEntity<List<M_Courier>> GetSearchCourier(@PathVariable("kata") String kata)
    {
        try{
            List<M_Courier> courier = this.repoCourier.search(kata);
            return new ResponseEntity<>(courier, HttpStatus.OK);
        }
        catch(Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/addcourier")
    public ResponseEntity<Object> SaveCourier(@RequestBody M_Courier courier){

        try
        {
            courier.setCreated_by(4L);
            courier.setCreated_on(new Date());
            this.repoCourier.save(courier);
            return new ResponseEntity<>(courier, HttpStatus.OK);
        }
        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/editcourier/{id}")
    public ResponseEntity<Object> UpdateCourier(@RequestBody M_Courier courier, @PathVariable("id") Long id)
    {
        Optional<M_Courier> courierData = this.repoCourier.findById(id);

        if (courierData.isPresent())
        {
            courier.setId(id);
            courier.setModified_by(1L);
            courier.setModified_on(new Date());
            this.repoCourier.save(courier);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/deletecourier/{id}") // jadikan delete agar tidak hapus permanen
    public ResponseEntity<Object> DeleteCourier(@PathVariable("id") Long id)
    {
        M_Courier courier = this.repoCourier.findByIdData(id);
        courier.setDeleted_by(1L);
        courier.setDeleted_on(new Date());
        courier.setCreated_on(new Date());
        courier.setIs_delete(true);
        this.repoCourier.save(courier);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

}
