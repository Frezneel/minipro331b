package com.team2.minipro331b.Minipro331b.controllerApi;


import com.team2.minipro331b.Minipro331b.model.M_Customer_Relation;
import com.team2.minipro331b.Minipro331b.repository.RepoCustomerRelation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestCustomerRelation {

    @Autowired
    private RepoCustomerRelation repoCustomerRelation ;

    @GetMapping("/customerrelation")
    public ResponseEntity<List<M_Customer_Relation>> getAllCustomerRelation(){
        try {
            List<M_Customer_Relation> m_customer_relation = this.repoCustomerRelation.findAllNotDeleted();
            return new ResponseEntity<>(m_customer_relation, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/customerrelation")
    public ResponseEntity<Object> SaveCustomerRelation(@RequestBody M_Customer_Relation m_customer_relation){
        try {
            m_customer_relation.setCreated_by(3L);
            m_customer_relation.setCreated_on(new Date());
            this.repoCustomerRelation.save(m_customer_relation);
            return new ResponseEntity<>(m_customer_relation, HttpStatus.OK);
        } catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/customerrelation/{id}")
    public ResponseEntity<List<M_Customer_Relation>> getCustomerRelationById(@PathVariable("id") Long id)
    {
        try {
            Optional<M_Customer_Relation> m_customer_relation = this.repoCustomerRelation.findById(id);
            if (m_customer_relation.isPresent()) {
                ResponseEntity rest = new ResponseEntity<>(m_customer_relation, HttpStatus.OK);
                return rest;
            }
            else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchcustomerrelation={kata}")
    public ResponseEntity<List<M_Customer_Relation>> GetSearchCustomerRelation(@PathVariable("kata") String kata)
    {
        try{
            List<M_Customer_Relation> customerRelation = this.repoCustomerRelation.search(kata);
            return new ResponseEntity<>(customerRelation, HttpStatus.OK);
        }
        catch(Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

//    @PutMapping("/customerrelation/{id}")
//    public ResponseEntity<Object> UpdateCustomerRelation(@RequestBody M_Customer_Relation m_customer_relation, @PathVariable("id") Long id)
//    {
//        Optional<M_Customer_Relation> m_customer_relationData= this.repoCustomerRelation.findById(id);
//
//        if (m_customer_relationData.isPresent())
//        {
//            m_customer_relation.setCreated_by(3L);
//            m_customer_relation.setCreated_on(new Date());
//            m_customer_relation.setModified_by(3L);
//            m_customer_relation.setModified_on(new Date());
//            this.repoCustomerRelation.save(m_customer_relation);
//            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
//            return rest;
//        } else {
//            return ResponseEntity.notFound().build();
//        }
//    }

    @PutMapping("/customerrelation/{id}")
    public ResponseEntity<Object> UpdateCustomerRelation(@RequestBody M_Customer_Relation m_customer_relation , @PathVariable("id") Long id) {
        Optional<M_Customer_Relation> m_customer_relationData = this.repoCustomerRelation.findById(id);

        if (m_customer_relationData.isPresent()) {
            M_Customer_Relation existingCustomerRelation = m_customer_relationData.get();

            Date createOn = existingCustomerRelation.getCreated_on();
            Long createdBy = existingCustomerRelation.getCreated_by();

            if (createOn != null && createdBy != null) {
                // Set only fields that are meant to be updated
                existingCustomerRelation.setName(m_customer_relation.getName());

                existingCustomerRelation.setModified_by(1L);
                existingCustomerRelation.setModified_on(new Date());

                this.repoCustomerRelation.save(existingCustomerRelation);
                ResponseEntity<Object> response = new ResponseEntity<>("Success", HttpStatus.OK);
                return response;
            } else {
                return ResponseEntity.badRequest().body("Nilai createOn atau createdBy kosong");
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }

//    @DeleteMapping("/customerrelation/{id}")
//    public ResponseEntity<Object> DeleteCustomerRelation(@PathVariable("id") Long id)
//    {
//        this.repoCustomerRelation.deleteById(id);
//        return new ResponseEntity<>("Success", HttpStatus.OK//    }

    @DeleteMapping("/customerrelation/{id}")
    public String DeleteCustomerRelation(@PathVariable("id") Long id)
    {
        try {
            M_Customer_Relation m_customer_relation  = this.repoCustomerRelation.findByIdData(id);
            m_customer_relation.setDeleted_by(3L);
            m_customer_relation.setDeleted_on(new Date());
            m_customer_relation.setIs_delete(true);
            this.repoCustomerRelation.save(m_customer_relation);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }
}
