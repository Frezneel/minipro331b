package com.team2.minipro331b.Minipro331b;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Minipro331bApplication {

	public static void main(String[] args) {
		SpringApplication.run(Minipro331bApplication.class, args);
	}

}
