package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.T_Token;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface RepoToken extends JpaRepository<T_Token, Long> {
    @Query(value = "SELECT * FROM t_token WHERE lower(t_token.email) = lower(:email) " +
            "AND lower(t_token.used_for) = lower(:used_for)" +
            "AND t_token.token = :input ORDER BY t_token.expired_on DESC LIMIT 1", nativeQuery = true)
    Optional<T_Token> getLastToken(String email, String used_for, String input);

    @Query(value = "SELECT * FROM t_token WHERE lower(t_token.email) = lower(:email) " +
            "AND lower(t_token.used_for) = lower(:used_for) ORDER BY t_token.expired_on DESC LIMIT 1", nativeQuery = true)
    T_Token getLastToken(String email, String used_for);


}
