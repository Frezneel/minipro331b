package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.M_Admin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepoAdmin extends JpaRepository<M_Admin, Long> {

}
