package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.Cari_Dokter;
import com.team2.minipro331b.Minipro331b.model.M_Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoCariDokter extends JpaRepository<Cari_Dokter, Long> {

    @Query(value = "SELECT * FROM cari_dokter WHERE is_delete = false",nativeQuery = true)
    List<Cari_Dokter> findAllNotDeleted();
}
