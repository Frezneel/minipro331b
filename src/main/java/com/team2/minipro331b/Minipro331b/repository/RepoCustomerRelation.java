package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.M_Customer_Relation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoCustomerRelation extends JpaRepository<M_Customer_Relation, Long> {
    @Query(value = "SELECT * FROM m_customer_relation WHERE is_delete = false",nativeQuery = true)
    List<M_Customer_Relation> findAllNotDeleted();

    @Query(value = "SELECT * FROM m_customer_relation WHERE lower(m_customer_relation.name) LIKE lower(concat('%',:kata,'%'))", nativeQuery = true)
    List<M_Customer_Relation> search(String kata);

    @Query(value = "SELECT * FROM m_customer_relation WHERE id = :id", nativeQuery = true)
    M_Customer_Relation findByIdData(long id);
}
