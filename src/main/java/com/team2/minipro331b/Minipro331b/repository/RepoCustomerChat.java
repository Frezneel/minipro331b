package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.T_Customer_Chat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepoCustomerChat extends JpaRepository<T_Customer_Chat, Long> {
}
