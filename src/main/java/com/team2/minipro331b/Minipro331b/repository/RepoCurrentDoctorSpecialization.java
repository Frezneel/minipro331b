package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.T_Current_Doctor_Specialization;
import com.team2.minipro331b.Minipro331b.model.T_Doctor_Office;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface RepoCurrentDoctorSpecialization extends JpaRepository<T_Current_Doctor_Specialization, Long> {

    @Query(value = "SELECT * FROM public.t_current_doctor_specialization WHERE is_delete = false", nativeQuery = true)
    List<T_Current_Doctor_Specialization> getAllCurrentDoctorSpecialization();

    @Query(value = "SELECT * FROM t_current_doctor_specialization WHERE t_current_doctor_specialization.doctor_id = :id", nativeQuery = true)
    Optional<T_Current_Doctor_Specialization> getSpecializationById(Long id);

}
