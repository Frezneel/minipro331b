package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.M_Specialization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;

import java.util.List;

public interface RepoSpecialization extends JpaRepository<M_Specialization, Long> {

    @Query( value = "select * FROM specialization WHERE lower(name) LIKE lower(concat('%',:keyword,'%'))", nativeQuery = true)
    List<M_Specialization> SearchSpecialization(String keyword);

    @Query(value = "SELECT * FROM specialization WHERE is_delete = false",nativeQuery = true)
    List<M_Specialization> findAllNotDeleted();

    @Query(value = "SELECT * FROM specialization WHERE id = :id", nativeQuery = true)
    M_Specialization findByIdData(long id);

    @Query(value = "SELECT * FROM specialization WHERE is_delete = false",nativeQuery = true)
    Page<M_Specialization> findAllNotDeleted(Pageable pageable);
}
