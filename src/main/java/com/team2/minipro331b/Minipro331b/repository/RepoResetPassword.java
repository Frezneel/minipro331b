package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.T_Reset_Password;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoResetPassword extends JpaRepository<T_Reset_Password, Long> {

    @Query(value = "SELECT * FROM public.t_reset_password WHERE is_delete = false AND created_by = :idUser", nativeQuery = true)
    List<T_Reset_Password> listValidasiPasswordByIdBiodata(Long idUser);
}
