package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.ProfilDoctor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepoProfilDoctor extends JpaRepository<ProfilDoctor, Long> {


}
