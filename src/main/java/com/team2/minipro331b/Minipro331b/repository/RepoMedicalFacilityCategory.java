package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.M_Location;
import com.team2.minipro331b.Minipro331b.model.M_Medical_Facility_Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoMedicalFacilityCategory extends JpaRepository<M_Medical_Facility_Category, Long> {

    @Query(value = "SELECT * FROM medical_facility_category WHERE is_delete = false",nativeQuery = true)
    List<M_Medical_Facility_Category> findAllNotDeleted();
}
