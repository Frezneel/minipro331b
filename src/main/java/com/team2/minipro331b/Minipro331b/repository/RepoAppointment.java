package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.T_Appointment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepoAppointment extends JpaRepository<T_Appointment, Long> {
}
