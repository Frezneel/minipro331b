package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.M_Doctor;
import com.team2.minipro331b.Minipro331b.model.M_Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoDoctor extends JpaRepository<M_Doctor, Long> {

    @Query(value = "SELECT * FROM doctor WHERE is_delete = false",nativeQuery = true)
    List<M_Doctor> findAllNotDeleted();
}
