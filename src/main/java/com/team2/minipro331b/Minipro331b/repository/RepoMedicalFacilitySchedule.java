package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.M_Medical_Facility_Schedule;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepoMedicalFacilitySchedule extends JpaRepository<M_Medical_Facility_Schedule, Long> {
}
