package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.M_Courier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoCourier extends JpaRepository<M_Courier, Long> {
    @Query(value = "SELECT * FROM m_courier ORDER BY m_courier ASC", nativeQuery = true)
    List<M_Courier> findAllCourierOrderbyASC();
    @Query(value = "SELECT * FROM m_courier WHERE is_delete = false ORDER BY m_courier.id ASC",nativeQuery = true)
    Page<M_Courier> findAllOrderByAsc(Pageable pageable);

    @Query(value = "SELECT * FROM m_courier WHERE m_courier.is_delete = false AND lower(m_courier.name) LIKE lower(concat('%',:kata,'%'))", nativeQuery = true)
    List<M_Courier> search(String kata);

    @Query(value = "SELECT * FROM m_courier WHERE id = :id", nativeQuery = true)
    M_Courier findByIdData(long id);

    @Query(value = "SELECT * FROM m_courier WHERE is_delete = false ORDER BY m_courier.id ASC", nativeQuery = true)
    List<M_Courier> findAllNotDeleted();

}
