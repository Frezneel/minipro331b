package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.M_Biodata;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface RepoBiodata extends JpaRepository<M_Biodata, Long> {
    @Query(value = "SELECT * FROM public.m_biodata WHERE m_biodata.id = :id", nativeQuery = true)
    Optional<M_Biodata> getBiodataById(Long id);
}
