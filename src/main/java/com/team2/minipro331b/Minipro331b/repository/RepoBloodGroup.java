package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.M_Blood_Group;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepoBloodGroup extends JpaRepository <M_Blood_Group, Long> {
}
