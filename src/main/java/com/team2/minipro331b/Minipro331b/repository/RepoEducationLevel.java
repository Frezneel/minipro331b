package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.M_Education_Level;
import com.team2.minipro331b.Minipro331b.model.M_Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoEducationLevel extends JpaRepository<M_Education_Level, Long> {

    @Query(value = "SELECT * FROM m_education_level WHERE is_delete = false",nativeQuery = true)
    List<M_Education_Level> findAllNotDeleted();

}
