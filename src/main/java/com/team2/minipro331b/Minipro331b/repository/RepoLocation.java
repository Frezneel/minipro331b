package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.M_Location;
import com.team2.minipro331b.Minipro331b.model.M_Specialization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoLocation extends JpaRepository<M_Location, Long> {

    @Query(value = "SELECT * FROM location WHERE is_delete = false",nativeQuery = true)
    List<M_Location> findAllNotDeleted();
}
