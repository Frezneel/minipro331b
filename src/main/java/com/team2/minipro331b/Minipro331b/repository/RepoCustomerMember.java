package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.M_Customer_Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepoCustomerMember extends JpaRepository<M_Customer_Member, Long> {
}
