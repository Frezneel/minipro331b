package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.M_User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface RepoUser extends JpaRepository<M_User, Long> {

    @Query(value = "SELECT * FROM public.m_user WHERE lower(m_user.email) = lower(:email)", nativeQuery = true)
    Optional<M_User> isEmailTerdaftar(String email);

    @Query(value = "SELECT * FROM public.m_user WHERE lower(m_user.email) = lower(:email) AND m_user.password = :password LIMIT 1", nativeQuery = true)
    M_User login(String email, String password);
}
