package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.M_Doctor_Education;
import com.team2.minipro331b.Minipro331b.model.T_Doctor_Office;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoDoctorEducation extends JpaRepository<M_Doctor_Education, Long> {

    @Query(value = "SELECT * FROM m_doctor_education WHERE is_delete = false",nativeQuery = true)
    List<M_Doctor_Education> findAllNotDeleted();

    @Query(value = "SELECT * FROM m_doctor_education WHERE is_delete = false AND doctor_id = :id_doctor", nativeQuery = true)
    List<M_Doctor_Education> findAllByIdEducation(Long id_doctor);
}
