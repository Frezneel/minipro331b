package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.M_Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoCustomer extends JpaRepository <M_Customer, Long> {
    @Query(value = "SELECT * FROM m_customer WHERE is_delete = false",nativeQuery = true)
    List<M_Customer> findAllNotDeleted();

    @Query(value = "SELECT * FROM m_customer WHERE lower(m_customer.name) LIKE lower(concat('%',:kata,'%'))", nativeQuery = true)
    List<M_Customer> search(String kata);

    @Query(value = "SELECT * FROM m_customer WHERE id = :id", nativeQuery = true)
    M_Customer findByIdData(long id);
}
