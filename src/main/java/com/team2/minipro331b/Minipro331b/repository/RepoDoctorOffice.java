package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.M_Location;
import com.team2.minipro331b.Minipro331b.model.T_Doctor_Office;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoDoctorOffice extends JpaRepository<T_Doctor_Office, Long> {

    @Query(value = "SELECT * FROM doctor_office WHERE is_delete = false",nativeQuery = true)
    List<T_Doctor_Office> findAllNotDeleted();
}
