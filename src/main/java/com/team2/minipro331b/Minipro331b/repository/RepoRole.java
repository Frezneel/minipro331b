package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.M_Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoRole extends JpaRepository<M_Role, Long> {

    @Query(value = "SELECT * FROM m_role ORDER BY m_role ASC", nativeQuery = true)
    List<M_Role> findAllRoleOrderbyASC();
    @Query(value = "SELECT * FROM m_role WHERE is_delete = false ORDER BY m_role.id ASC",nativeQuery = true)
    Page<M_Role> findAllOrderByAsc(Pageable pageable);

    @Query(value = "SELECT * FROM m_role WHERE m_role.is_delete = false",nativeQuery = true)
    Page<M_Role> findAllOrder(Pageable pageable);

    @Query(value = "SELECT * FROM m_role WHERE lower(m_role.name) LIKE lower(concat('%',:kata,'%'))", nativeQuery = true)
    List<M_Role> search(String kata);

    @Query(value = "SELECT * FROM m_role WHERE id = :id", nativeQuery = true)
    M_Role findByIdData(long id);

    @Query(value = "SELECT * FROM m_role WHERE is_delete = false ORDER BY m_role.id ASC", nativeQuery = true)
    List<M_Role> findAllNotDeleted();

    @Query(value = "SELECT * FROM m_role WHERE m_role.is_delete = false AND lower(m_role.name) LIKE lower (concat('%',:kata,'%'))", nativeQuery = true)
    Page<M_Role> searchMapped(Pageable pageable, String kata);

}
