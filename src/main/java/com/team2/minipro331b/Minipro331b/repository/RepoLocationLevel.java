package com.team2.minipro331b.Minipro331b.repository;

import com.team2.minipro331b.Minipro331b.model.M_Level_Location;
import com.team2.minipro331b.Minipro331b.model.M_Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoLocationLevel extends JpaRepository<M_Level_Location, Long> {

    @Query(value = "SELECT * FROM level_location WHERE is_delete = false",nativeQuery = true)
    List<M_Level_Location> findAllNotDeleted();
}
